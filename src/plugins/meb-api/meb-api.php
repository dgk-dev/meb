<?php
/**
* Plugin Name: MEB API
* Description: Endpoints personalizados para la app de Morelia en Boca.
* Version: 0.3
* Author: dgk
* Author URI: http://dgk.com.mx/
**/

/**
 * INIT
 */
define('MEB_API_URL', plugin_dir_url( __FILE__ ));
// Carga de configuración de healess wordpress
include(plugin_dir_path( __FILE__ ) . 'includes/headless-config.php');

// Carga de API revslider 
include(plugin_dir_path( __FILE__ ) . 'revslider-api/revslider-api.php');

// Carga de Boletia
include(plugin_dir_path( __FILE__ ) . 'boletia-api/boletia-api.php');
register_activation_hook( __FILE__, 'meb_create_boletia_tables' );

// Carga de Usuarios
include(plugin_dir_path( __FILE__ ) . 'users-api/users-api.php');

// Carga de Eventos
include(plugin_dir_path( __FILE__ ) . 'events-api/events-api.php');