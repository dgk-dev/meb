<?php

class MEB_Users_Controller extends WP_REST_Controller
{

	public function __construct() {

		//set filter for responses
        add_filter( 'wp_headless_rest__generate_jwt_response', array($this,'wp_rest_headless_filter_jwt_response') , 10, 3);
    }
    
    public function wp_rest_headless_filter_jwt_response( $response, $token, $user ) {

        $response = array(
            'code' => 'user_login_success',
            'message' => 'Se ha ingresado con éxito',
            'token' => $token,
            'user'  => array(
                'user_email'    => $user->data->user_email,
                'user_id'    => $user->ID,
            )
        );
    
        return $response;
    }

    public function register_routes()
    {
        $namespace = 'meb/v1';
        $path = 'users';

        register_rest_route($namespace, '/' . $path . '/register', [
            array(
                'methods'             => 'POST',
                'callback'            => array($this, 'register_user'),
                'permission_callback' => array($this, 'register_user_permissions_check')
            ),

        ]);

        register_rest_route($namespace, '/' . $path . '/login', [
            array(
                'methods'             => 'POST',
                'callback'            => array($this, 'login_user'),
                'permission_callback' => array($this, 'login_user_permissions_check')
            ),

        ]);

        register_rest_route($namespace, '/' . $path . '/lost_password', [
            array(
                'methods'             => 'POST',
                'callback'            => array($this, 'lost_password_user'),
                'permission_callback' => array($this, 'login_user_permissions_check')
            ),

        ]);
    }

    public function register_user_permissions_check($request)
    {
        return true;
    }

    public function register_user($request)
    {
        $parameters = $request->get_json_params();
        
        $name = sanitize_text_field($parameters['name']);
		$password = sanitize_text_field($parameters['password']);
		$password_repeat = sanitize_text_field($parameters['password_repeat']);
		$email = sanitize_text_field($parameters['email']);
        $phone = sanitize_text_field($parameters['phone']);
        $codes = array(
            'code1' => sanitize_text_field($parameters['code1']),
            'code2' => sanitize_text_field($parameters['code2']),
            'code3' => sanitize_text_field($parameters['code3'])

        );
        $errors = array();
        $response= array();
        
        //Validate name
        if(empty($name)){
            $errors [] = array(
                'field' => 'name',
                'error' => 'El campo es requerido'
            );
        }

        //validate password
        if(empty($password)){
            $errors [] = array(
                'field' => 'password',
                'error' => 'El campo es requerido'
            );
        }elseif(strlen(utf8_decode($password)) < 5 || strlen(utf8_decode($password)) > 10){
            $errors [] = array(
                'field' => 'password',
                'error' => 'El password debe tener entre 5 y 10 caracteres'
            );
        }elseif($password != $password_repeat){
            $errors [] = array(
                'field' => 'password_repeat',
                'error' => 'los passwords no coinciden'
            );
        }
        
        //validate phone
        if(empty($phone)){
            $errors [] = array(
                'field' => 'phone',
                'error' => 'El campo es requerido'
            );
        }elseif(!is_numeric($phone) || strlen(utf8_decode($phone)) != 10){
            $errors [] = array(
                'field' => 'phone',
                'error' => 'El teléfono tiene que ser de 10 dígitos'
            );
        }

        //validate email
        if(empty($email)){
            $errors [] = array(
                'field' => 'email',
                'error' => 'El campo es requerido'
            );
        }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors [] = array(
                'field' => 'email',
                'error' => 'El email no tiene un formato válido'
            );
        }elseif(email_exists($email)){
            $errors [] = array(
                'field' => 'email',
                'error' => 'El email ya se encuentra en uso'
            );
        }

        //validate codes
        global $wpdb;
        $tickets_table = $wpdb->prefix . 'boletia_tickets';
        foreach($codes as $key => $code){
            if(!empty($code)){
                $ticket = $wpdb->get_row("SELECT * FROM $tickets_table WHERE ticket_id = \"$code\"");
                if(!$ticket){
                    $errors [] = array(
                        'field' => $key,
                        'error' => 'El código no existe'
                    );
                }elseif($ticket->user_id){
                    $errors [] = array(
                        'field' => $key,
                        'error' => 'El código ya está siendo usado'
                    );
                }
            }else{
                unset($codes[$key]);
            }
        }
        if(empty($codes)){
            $errors [] = array(
                'field' => 'code1',
                'error' => 'Debes introducir al menos un código'
            );
        }

        if(!empty($errors)){
            $response = new WP_REST_Response(array(
                'code' => 'user_register_validation_error',
                'message' => 'Uno o más campos tienen errores de validación',
                'data' => $errors
            ));
            $response->set_status(400);
            return $response;
        }

        // Creación de usuario
    
        // Generar username
        $username = strstr($email, '@', true);
        if (username_exists($username)) {
            $suffix = 2;
            while (username_exists($username)) {
                $username = $username . '-' . $suffix;
                $suffix++;
            }
        }

        $user_id = wp_create_user($username, $password, $email);

        if (!is_wp_error($user_id)) {
            //asignar códigos al usuario
            $this->assign_tickets($user_id, $codes);
            $user_response = $this->login_user($request);
            if(!is_wp_error($user_response)){
                $response = new WP_REST_Response(array(
                    'code' => 'user_register_success',
                    'message' => 'El usuario se ha registrado con éxito',
                    'token' => $user_response->data['token'],
                    'user' => array(
                        'user_email' => $email,
                        'user_id' => $user_id
                    )
                ));
                $response->set_status(200);

            }
        } else {
            $response = new WP_REST_Response(array(
                'code' => 'user_register_error',
                'message' => 'Ha ocurrido un error',
                'data' => $user_id->get_error_message()
            ));
            $response->set_status(400);
        }
        return $response;
    }

    function assign_tickets($user_id, $ticket_ids){
        global $wpdb;
        $tickets_table = $wpdb->prefix . 'boletia_tickets';
        $tickets = is_array($ticket_ids) ? $ticket_ids : array($ticket_ids); 
        $success = false;
        foreach($tickets as $ticket_id){
            $success = $wpdb->update(
                $tickets_table,
                array('user_id' => $user_id),
                array('ticket_id' => $ticket_id),
                array('%d'),
                array('%s')
            );
            if(!$success) break;
        }
        return $success;
    }

    public function login_user_permissions_check($request)
    {
        return true;
    }

    public function login_user($request)
    {
        $parameters = $request->get_json_params();
        $username = $parameters['email'] ? sanitize_text_field($parameters['email']) : sanitize_text_field($parameters['username']);
		$password = sanitize_text_field($parameters['password']);
        $request = new WP_REST_Request( 'POST', '/wp-headless/v1/jwt/get-token' );
        $request->set_query_params( [ 'username' => $username, 'password' => $password ] );
        $response = rest_do_request( $request );
        if($response->status == 403){
            $response = new WP_REST_Response(array(
                'code' => 'user_login_error',
                'message' => 'Ha ocurrido un error al ingresar, por favor revisa tu email y contraseña',
            ));
            $response->set_status($response->status);
        }
        return $response;
    }

    public function lost_password_user($request = null)
    {

		$response = array();
		$parameters = $request->get_json_params();
		$user_email = sanitize_text_field($parameters['email']);
		$errors = array();

		if (empty($user_email)) {
			$errors[] = array('El campo email es requerido');
        }elseif(!email_exists($user_email)){
			$errors[] = array('No existe usuario con la dirección de correo indicada');
        }
        
        if(!empty($errors)){
            $response = new WP_REST_Response(array(
                'code' => 'lost_password_error',
                'message' => 'Ha ocurrido un error al recuperar el password',
                'data' => $errors
            ));
            $response->set_status(400);
            return $response;
        }
        
		// run the action
		do_action('retrieve_password', $user_email);

		$response = new WP_REST_Response(array(
            'code' => 'lost_password_email_sent',
            'message' => 'Se ha enviado un correo electrónico para recuperar tu clave',
            'data' => array(
                'user_email' => $user_email
            )
        ));
        $response->set_status(200);

        return $response;
	}
}
