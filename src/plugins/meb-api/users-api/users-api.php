<?php
require('users-controller.php');

add_action('rest_api_init', function(){
    //load data controller
    $meb_users_controller = new MEB_Users_Controller();
    $meb_users_controller->register_routes();
});