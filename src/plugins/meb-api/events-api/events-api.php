<?php
require('events-controller.php');
require('events-venue-map.php');

add_action('rest_api_init', function(){
    //load data controller
    $meb_events_controller = new MEB_Events_Controller();
    $meb_events_controller->register_routes();
});