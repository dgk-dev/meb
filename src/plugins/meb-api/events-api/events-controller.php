<?php

class MEB_Events_Controller extends WP_REST_Controller
{

    public function register_routes()
    {
        $namespace = 'meb/v1';
        $path = 'events';

        register_rest_route($namespace, '/' . $path , [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_events'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/(?P<event_id>\d+)', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_single_event'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/venues', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_venues'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/venues/(?P<venue_id>\d+)', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_single_venue'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/organizers', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_organizers'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/menus', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_menus'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/menus/(?P<menu_id>\d+)', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_single_menu'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/guests', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_guests'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);

        register_rest_route($namespace, '/' . $path . '/guests/(?P<guest_id>\d+)', [
            array(
                'methods'             => 'GET',
                'callback'            => array($this, 'get_single_guest'),
                'permission_callback' => array($this, 'get_events_permissions_check')
            ),
        ]);
    }

    public function get_events_permissions_check($request)
    {
        return true;
        return is_user_logged_in();
    }

    public function get_events($request)
    {
        $parameters = $request->get_query_params();
        $event_args = array(
            'start_date'     => 'now',
            // 'end_date'     => date('Y-m-d 23:59:59'), //solo los del día
            'posts_per_page' => -1,
            'tax_query'=> array(
                array(
                    'taxonomy' => 'tribe_events_cat',
                    'field' => 'slug',
                    'terms' => 'menu',
                    'operator' => 'NOT IN'
                )
            )
        );

        if($parameters['menu_id']){
            $event_args['meta_query'] = array(
                array(
                    'key'     => '_tribe_linked_post_tribe_menu',
                    'value'   => $parameters['menu_id'],
                    'compare' => 'IN',
                ),
            );
        }
        if($parameters['venue_id']){
            $event_args['meta_query'] = array(
                array(
                    'key'     => '_EventVenueID',
                    'value'   => $parameters['venue_id'],
                    'compare' => 'IN',
                ),
            );
        }

        $tribe_events = tribe_get_events($event_args);
        $events = array();
        foreach($tribe_events as $event){
            $events[] = $this->get_event_data($event->ID);
        }

        if(empty($events)){
            $response = new WP_REST_Response(array(
                'code' => 'no_events',
                'message' => 'No se encontraron eventos',
            ));
            $response->set_status(404);
            return $response;
        }
        return $events;
    }

    public function get_single_event($request){
        $event_id = $request['event_id'];
        if(!tribe_is_event($event_id)){
            $response = new WP_REST_Response(array(
                'code' => 'no_event',
                'message' => 'No se encontró el evento',
            ));
            $response->set_status(404);
            return $response;
        }
        return $this->get_event_data($event_id);
    }

    
    public function get_event_data($event_id){
        remove_filter('get_the_excerpt', 'wp_trim_excerpt');
        $tribe_event = tribe_events_get_event($event_id);
        // obtener lugar del evento
        $venue_id = tribe_get_venue_id($tribe_event->ID);
        $venue = $venue_id ? $this->get_venue_data($venue_id) : array();
        

        //obtener menus del evento
        $tribe_menus = tribe_get_linked_posts_by_post_type( $tribe_event->ID, 'tribe_menu' );
        $menus = array();
        foreach($tribe_menus as $menu){
            $menus[] = $this->get_menu_data($menu->ID);
        }


        // obtener invitados de vino del evento
        $tribe_guests = tribe_get_linked_posts_by_post_type( $tribe_event->ID, 'tribe_guest' );
        $guests = array();
        foreach($tribe_guests as $guest){
            $guests[] = $this->get_guest_data($guest->ID);
        }
        $date_format = 'd-m-Y H:i:s';
        $start_date = tribe_get_start_date($tribe_event->ID, true, $date_format);
        $end_date = tribe_get_end_date($tribe_event->ID, true, $date_format);
        $start_date_obj = DateTime::createFromFormat($date_format, $start_date);
        $end_date_obj = DateTime::createFromFormat($date_format, $end_date);

        $terms = wp_get_post_terms( $event_id, Tribe__Events__Main::TAXONOMY );
        $event_categories = array();
        foreach( $terms as $term ) {
            $event_categories []= $term->name;
        }

        $event = array(
            'id' => $tribe_event->ID,
            'name' => $tribe_event->post_title,
            'permalink' => get_permalink($event_id),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'date_data' => array(
                'start_date' => array(
                    'day' => date_i18n('D d', $start_date_obj->getTimestamp()),
                    'hour' => $start_date_obj->format('H:i'),
                ),
                'end_date' => array(
                    'day' => date_i18n('D d', $end_date_obj->getTimestamp()),
                    'hour' => $end_date_obj->format('H:i'),
                ),
            ),
            'description' => $tribe_event->post_content,
            'excerpt' => get_the_excerpt($tribe_event->ID),
            'categories' => !empty($event_categories) ? $event_categories : array(),
            'image' => get_the_post_thumbnail_url($tribe_event->ID, 'full'),
            'venue' => $venue,
            'menus' => $menus,
            'guests' => $guests
        );
        return $event;

    }

    public function get_venue_data($venue_id){
        $tribe_venue = tribe_get_venue_object($venue_id);
        $venue = array();
        if($tribe_venue){
            unset($tribe_venue->geolocation->overwrite_coordinates);
            unset($tribe_venue->geolocation->distance);

            $events_args = array(
                'eventDisplay' => 'all',
                'meta_query' => array(
                    array(
                        'key'     => '_EventVenueID',
                        'value'   => $venue_id,
                        'compare' => 'IN',
                    ),
                ),
                'tax_query'=> array(
                    array(
                        'taxonomy' => 'tribe_events_cat',
                        'field' => 'slug',
                        'terms' => 'menu'
                    )
                )
            );

            $events = tribe_get_events($events_args);

            $day_menus = array();

            foreach($events as $event){
                $tribe_menus = tribe_get_linked_posts_by_post_type( $event->ID, 'tribe_menu' );
                foreach($tribe_menus as $menu){
                    $day_menus[] = $this->get_menu_data($menu->ID);
                }
            }

            $venue = array(
                'id' => $tribe_venue->ID,
                'name' => $tribe_venue->post_title,
                'permalink' => tribe_get_venue_link($venue_id, false),
                'description' => $tribe_venue->post_content,
                'geolocation' => $tribe_venue->geolocation,
                'day_menus' => $day_menus
            );
        }
        return $venue;
    }

    public function get_menu_data($menu_id){
        $menu_post= get_post($menu_id);
        $menu = array(
            'id' => $menu_post->ID,
            'name' => $menu_post->post_title,
            'permalink' => get_permalink($menu_post->ID),
            'image' => get_the_post_thumbnail_url($menu_post->ID, 'full'),
            'description' => $menu_post->post_content,
            'video' => get_post_meta($menu_post->ID, '_tribe_menu_video', true),
        );
        return $menu;
    }

    public function get_guest_data($guest_id){
        $guest_post= get_post($guest_id);
        $guest = array(
            'id' => $guest_post->ID,
            'name' => $guest_post->post_title,
            'permalink' => get_permalink($guest_post->ID),
            'type' => get_post_meta($guest_post->ID, '_tribe_guest_tipo', true),
            'origin' => get_post_meta($guest_post->ID, '_tribe_guest_procedencia', true),
            'image' => get_the_post_thumbnail_url($guest_post->ID, 'full'),
            'description' => $guest_post->post_content,
        );
        return $guest;
    }

    public function get_venues($request){
        $tribe_venues = tribe_get_venues();
        $venues = array();
        foreach($tribe_venues as $key => $tribe_venue){
            $venues[] = $this->get_venue_data($tribe_venue->ID);
            $venues[$key]['related_events'] = $this->get_related_events('_EventVenueID', $tribe_venue->ID);
        }
        if(empty($venues)){
            $response = new WP_REST_Response(array(
                'code' => 'no_venues',
                'message' => 'No se encontraron sedes',
            ));
            $response->set_status(404);
            return $response;
        }
        return $venues;
    }

    public function get_single_venue($request){
        $venue_id = $request['venue_id'];
        $venue =  $this->get_venue_data($venue_id);
        if(empty($venue)){
            $response = new WP_REST_Response(array(
                'code' => 'no_venue',
                'message' => 'No se encontró la sede',
            ));
            $response->set_status(404);
            return $response;
        }
        $venue['related_events'] = $this->get_related_events('_EventVenueID', $venue_id);
        return $venue;
    }

    public function get_related_events($key, $value){
        $events_args = array(
            'eventDisplay' => 'upcoming',
            'meta_query' => array(
                array(
                    'key'     => $key,
                    'value'   => $value,
                    'compare' => 'IN',
                ),
            ),
            'tax_query'=> array(
                array(
                    'taxonomy' => 'tribe_events_cat',
                    'field' => 'slug',
                    'terms' => 'menu',
                    'operator' => 'NOT IN'
                )
            )
        );
        
        $events = array();
        
        $tribe_events = tribe_get_events($events_args);
        foreach($tribe_events as $key => $event){
            $events[] = $this->get_event_data($event->ID);
        }

        return $events;
    }

    public function get_all_related_events($key, $value){
        $events_args = array(
            'eventDisplay' => 'all',
            'meta_query' => array(
                array(
                    'key'     => $key,
                    'value'   => $value,
                    'compare' => 'IN',
                ),
            )
        );
        
        $events = array();
        
        $tribe_events = tribe_get_events($events_args);
        foreach($tribe_events as $key => $event){
            $events[] = $this->get_event_data($event->ID);
        }

        return $events;
    }

    public function get_organizers($request){
        $tribe_organizers = tribe_get_organizers();
        $organizers = array();
        foreach($tribe_organizers as $tribe_organizer){
            $organizers[] = $this->get_organizer_data($tribe_organizer->ID);
        }
        if(empty($organizers)){
            $response = new WP_REST_Response(array(
                'code' => 'no_organizers',
                'message' => 'No se encontraron patrocinadores',
            ));
            $response->set_status(404);
            return $response;
        }
        return $organizers;
        
    }

    public function get_organizer_data($organizer_id){
        $organizer_post= get_post($organizer_id);
        $organizer = array(
            'id' => $organizer_post->ID,
            'name' => $organizer_post->post_title,
            'image' => get_the_post_thumbnail_url($organizer_post->ID, 'full'),
            'website' => get_post_meta($organizer_post->ID, '_OrganizerWebsite', true)
        );
        return $organizer;
    }

    public function get_menus($request){
        $menu_args = array(
            'post_type'=> 'tribe_menu',
            'posts_per_page' => -1,
            'order' => 'ASC'
        );
        $tribe_menus = get_posts($menu_args);
        $menus = array();
        foreach($tribe_menus as $key => $tribe_menu){
            $menus[] = $this->get_menu_data($tribe_menu->ID);
            $menus[$key]['related_events'] = $this->get_all_related_events('_tribe_linked_post_tribe_menu', $tribe_menu->ID);
        }
        if(empty($menus)){
            $response = new WP_REST_Response(array(
                'code' => 'no_menus',
                'message' => 'No se encontraron menus',
            ));
            $response->set_status(404);
            return $response;
        }
        return $menus;
    }

    public function get_single_menu($request){
        $menu_id = $request['menu_id'];
        $menu =  $this->get_menu_data($menu_id);
        if(empty($menu)){
            $response = new WP_REST_Response(array(
                'code' => 'no_menu',
                'message' => 'No se encontró el menu',
            ));
            $response->set_status(404);
            return $response;
        }
        $menu['related_events'] = $this->get_related_events('_tribe_linked_post_tribe_menu', $menu_id);
        $events = $this->get_all_related_events('_tribe_linked_post_tribe_menu', $menu_id);
        
        $guests = array();
        $venues = array();
        foreach($events as $event){
            if(isset($event['venue']['id']) && !in_array($event['venue']['id'], $venues))
                $venues[$event['venue']['id']] = $this->get_venue_data($event['venue']['id']);
            foreach($event['guests'] as $guest){
                if(!in_array($guest['id'], $guests))
                    $guests[$guest['id']] = $guest;
            }
        }
        $menu['related_guests'] = $guests;
        $menu['related_venues'] = $venues;
        return $menu;
    }


    public function get_guests($request){
        $guest_args = array(
            'post_type'=> 'tribe_guest',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'title'
        );
        $tribe_guests = get_posts($guest_args);
        $guests = array();
        foreach($tribe_guests as $key => $tribe_guest){
            $guests[] = $this->get_guest_data($tribe_guest->ID);
            $guests[$key]['related_events'] = $this->get_related_events('_tribe_linked_post_tribe_guest', $tribe_guest->ID);
        }
        if(empty($guests)){
            $response = new WP_REST_Response(array(
                'code' => 'no_guests',
                'message' => 'No se encontraron invitados de vino',
            ));
            $response->set_status(404);
            return $response;
        }
        return $guests;
    }

    public function get_single_guest($request){
        $guest_id = $request['guest_id'];
        $guest =  $this->get_guest_data($guest_id);
        if(empty($guest)){
            $response = new WP_REST_Response(array(
                'code' => 'no_guest',
                'message' => 'No se encontró el invitado de vino',
            ));
            $response->set_status(404);
            return $response;
        }
        $guest['related_events'] = $this->get_related_events('_tribe_linked_post_tribe_guest', $guest_id);
        $events = $this->get_all_related_events('_tribe_linked_post_tribe_guest', $guest_id);
        
        $menus = array();
        foreach($events as $event){
            foreach($event['menus'] as $menu){
                $menus[$menu['id']]= $menu;
                $menus[$menu['id']]['venues'][]= isset($event['venue']['id']) ? $this->get_venue_data($event['venue']['id']) : array();
            }
        }
        $guest['related_menus'] = $menus;
        return $guest;
    }
}
