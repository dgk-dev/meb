<?php

// Add assets
function meb_event_venue_map_assets()
{
    $screen = get_current_screen();
    if ($screen->id == 'tribe_venue') {
        $gmap_api_key = tribe_get_option('google_maps_js_api_key', Tribe__Events__Google__Maps_API_Key::$default_api_key);
        wp_enqueue_script('google-maps-native', '//maps.googleapis.com/maps/api/js?key=' . $gmap_api_key . '&libraries=places');
        wp_enqueue_script('map-script', MEB_API_URL . 'assets/js/tribe-venue-map.js');
    }
}
add_action('admin_enqueue_scripts', 'meb_event_venue_map_assets', 50);

// Create Metabox
function meb_event_venue_map_add_embed_gmaps_meta_box()
{
    add_meta_box(
        'gmaps_embed_meta_box', // $id
        'Ubicación', // $title
        'meb_event_venue_map_show_embed_gmaps_meta_box', // $callback
        'tribe_venue', // $page
        'normal', // $context
        'high'
    ); // $priority
}
add_action('add_meta_boxes', 'meb_event_venue_map_add_embed_gmaps_meta_box');

// Show Metabox Contents
function meb_event_venue_map_show_embed_gmaps_meta_box()
{
    global $post;
    $hidden_venue_capacity = get_post_meta($post->ID, 'venue_capacity', true);
    $nonce = wp_create_nonce(basename(__FILE__));
?>
    <style>
        #map-canvas {
            height: 50vh;
            width: 100%;
        }

        #ep-map-data input {
            width: 100%;
        }

        #ep-map-data textarea {
            width: 100%;
        }

        #map-search input,
        #map-search button {
            width: 100%;
        }
    </style>
    <table style="width: 100%;">
        <tbody class="form-table" id="map-search">
            <tr>
                <th style="font-weight:normal">
                    <input id="map-search-input" type="text" placeholder="Buscar en el mapa" />
                </th>
                <td style="width:20%; text-align: center">
                    <button class="button button-primary" id="map-search-button">Buscar</button>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="maparea" id="map-canvas"></div>
    <input type="hidden" name="meb_map_meta_box_nonce" value="<?php echo $nonce; ?>">
    <input type="hidden" id="hidden_venue_capacity" name="hidden_venue_capacity" value="<?php echo $hidden_venue_capacity; ?>">
<?php
}

// Save Metaboxes.
function meb_event_venue_map_save_embed_gmap($post_id)
{
    // verify nonce
    if (!wp_verify_nonce($_POST['meb_map_meta_box_nonce'], basename(__FILE__)))
        return $post_id;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    $oldcapacity = get_post_meta($post_id, "venue_capacity", true);
    $newcapacity = $_POST["venue_capacity"];
    if ($newcapacity != $oldcapacity) {
        update_post_meta($post_id, "venue_capacity", $newcapacity);
    }
}
add_action('save_post', 'meb_event_venue_map_save_embed_gmap');

function tribe_remove_organizers_from_events($default_types)
{

    if (
        !is_array($default_types)
        || empty($default_types)
        || empty(Tribe__Events__Main::ORGANIZER_POST_TYPE)
    ) {
        return $default_types;
    }

    if (($key = array_search(Tribe__Events__Main::ORGANIZER_POST_TYPE, $default_types)) !== false) {
        unset($default_types[$key]);
    }

    return $default_types;
}

add_filter('tribe_events_register_default_linked_post_types', 'tribe_remove_organizers_from_events');
