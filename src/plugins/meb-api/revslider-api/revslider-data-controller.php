<?php

class MEB_Revslider_Data_Controller extends WP_REST_Controller {

    public function register_routes() {
        $namespace = 'meb/v1';
        $path= 'revslider-data';
    
        register_rest_route( $namespace, '/' . $path, [
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_items' ),
                'args' => array(
                    'revslider_alias' => array( 
                        'validate_callback' => function( $param, $request, $key ) {
                            return true;
                        }
                    ),
                ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            ),
        ]);     
    }

    public function get_items_permissions_check($request) {
        return current_user_can( 'edit_others_posts' );
    }

    public function get_items($request){

        if(!class_exists("RevSlider")){
            return new WP_Error( 'plugin_error', 'el plugin revslider no esta activado', array('status' => 404) );
        }

        $RevSlider = new RevSlider();
        $revslider_alias = $request['revslider_alias'];

        if(!$RevSlider->alias_exists($revslider_alias)) return new WP_Error( 'alias_error', 'no se ha encontrado slider con ese alias', array('status' => 404) );

        $RevSlider->init_by_alias($revslider_alias);

        $slides = $RevSlider->get_slides();

        $slides_data = array();
        
        if(!empty($slides)){
            foreach($slides as $slide){
                $slide_num = $slide->get_order();
                $layers = $slide->get_layers();
                if(!empty($layers)){
                    $filtered_layers = array_filter($layers, function ($layer) {
                        if (($layer['type'] == 'image' && ($layer['alias'] == 'mobile' || $layer['alias'] == 'Mobile')) || $layer['type'] == 'video') return true;
                    });
                    if(!empty($filtered_layers)){
                        foreach($filtered_layers as $filtered_layer){
                            switch($filtered_layer['type']){
                                case 'image':
                                    $actions = $filtered_layer['actions']['action'] ? $filtered_layer['actions']['action'] : array();
                                    $image_link = '';

                                    foreach($actions as $action){
                                        if($action['action'] && $action['action'] == 'link'){
                                            $image_link = $action['image_link'];
                                            break;
                                        }
                                    }
                                    $slide_data = array(
                                        'slide_num' => $slide_num,
                                        'type' => $filtered_layer['type'],
                                        'mobile_img_url' => $filtered_layer['media']['imageUrl'] ? $filtered_layer['media']['imageUrl'] : '',
                                        'image_link' => $image_link
                                    );
                                    break;
                                case 'video':
                                    $slide_data = array(
                                        'slide_num' => $slide_num,
                                        'type' => $filtered_layer['type'],
                                        'video_type' => $filtered_layer['media']['mediaType'] ? $filtered_layer['media']['mediaType'] : '',
                                        'video_id' => $filtered_layer['media']['id'] ? $filtered_layer['media']['id'] : '',
                                    );
                                    break;
                                default:
                                    $slide_data = array();
                            }
                            if(!empty($slide_data)) $slides_data[] = $slide_data;
                        }
                    }
                }
            }
        }

        if(empty($slides_data)) return new WP_Error( 'slides_error', 'no se encontraron slides', array('status' => 404) );

        $response = new WP_REST_Response($slides_data);
        $response->set_status(200);

        return $response;
    }
}