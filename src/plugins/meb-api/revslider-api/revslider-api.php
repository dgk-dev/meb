<?php

//Carga de controlador y rutas de revslider
require('revslider-data-controller.php');

add_action('rest_api_init', function(){
    //load data controller
    $meb_revslider_data_controller = new MEB_Revslider_Data_Controller();
    $meb_revslider_data_controller->register_routes();
});