<?php
/**
 * API METHODS
 */

 # NONCE - see the REST Nonce docs for examples on how to generate a fresh nonce key
// add_filter( 'wp_headless_rest__enable_rest_nonce', '__return_true' );

//Enable this on your DEV environment to allow rest request without the nonce
add_filter( 'wp_headless_rest__disable_rest_nonce_check', '__return_true' );

# REST CLEANUP - see the REST Cleanup docs in plugin settings as you may want to configure this beyond default
add_filter( 'wp_headless_rest__enable_rest_cleanup', '__return_true' );

# Gutenberg - see REST Gutenberg docs as you will need to register the post types you want to use this with
// add_filter( 'wp_headless_rest__enable_rest_gutenberg_blocks', '__return_true' );

/**
 * Checkout the JWT Authenication docs as well as the request example docs:
 *
 * @link https://documenter.getpostman.com/view/1175138/RztkPpV4#fc567f81-ff31-442e-a236-fa247bd1fd6c
 *
 */
add_filter( 'wp_headless_rest__enable_jwt', '__return_true' );

# CORS Security Headers - in most cases you can use the defaults but see the docs if you want to customise
add_filter( 'wp_headless_rest__cors_rules', 'wp_rest_headless_header_rules' );
function wp_rest_headless_header_rules( $rules ) {

	$rules = array(
		'Access-Control-Allow-Origin'      => '*',
		'Access-Control-Allow-Methods'     => 'GET, POST',
		'Access-Control-Allow-Credentials' => 'true',
		'Access-Control-Allow-Headers'     => 'Access-Control-Allow-Headers, Content-Type, Authorization, origin, x-wp-nonce, x-wp-nonce-generator',
		'Access-Control-Expose-Headers'    => array( 'Link', false ), //Use array if replace param is required
	);

	return $rules;
}

# You can disable REST api enpoints that you are not using and hide them from public access:
add_filter( 'wp_headless_rest__rest_endpoints_to_remove', 'wp_rest_headless_disable_endpoints' );
function wp_rest_headless_disable_endpoints( $endpoints_to_remove ) {

	$endpoints_to_remove = array(
		'/wp/v2/media',
		'/wp/v2/types',
		'/wp/v2/statuses',
		'/wp/v2/taxonomies',
		'/wp/v2/tags',
		'/wp/v2/users',
		'/wp/v2/comments',
		'/wp/v2/settings',
		'/wp/v2/themes',
		'/wp/v2/blocks',
		'/wp/v2/block-renderer',
		'/oembed/',
		//JETPACK
		'jp_pay_product',
		'jp_pay_order',
	);


	return $endpoints_to_remove;
}

# Select which post types to clean:
add_filter( 'wp_headless_rest__post_types_to_clean', 'wp_rest_headless_clean_post_types' );
function wp_rest_headless_clean_post_types( $post_types_to_clean ) {

	$post_types_to_clean = array(
		'post',
		'page',
	);

	return $post_types_to_clean;
}

# Select what to remove from the response object:
add_filter( 'wp_headless_rest__rest_object_remove_nodes', 'wp_rest_headless_clean_response_nodes' );
function wp_rest_headless_clean_response_nodes( $items_to_remove ) {

	$items_to_remove = array(
		'guid',
		'_links',
		'ping_status',
	);

	return $items_to_remove;
}

# All requests to any front end page (except the wp admin/login) will return 404.
add_filter( 'wp_headless_rest__disable_front_end', '__return_false' );
