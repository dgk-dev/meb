<?php

//Carga de controlador y rutas de boletia
require('boletia-controller.php');

add_action('rest_api_init', function(){
    //load data controller
    $meb_boletia_controller = new meb_boletia_Controller();
    $meb_boletia_controller->register_routes();
});

function meb_create_boletia_tables(){
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $orders_table_name = $wpdb->prefix . "boletia_orders";
    $sql_orders = "CREATE TABLE $orders_table_name (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    order_id VARCHAR(50) NULL,
    type VARCHAR(50) NULL,
    status VARCHAR(50) NULL,
    created_at VARCHAR(50) NULL,
    relationships TEXT NULL,
    raw_json TEXT NULL,
    created VARCHAR(255) NULL,
    updated VARCHAR(255) NULL,
    PRIMARY KEY  (id)
    ) $charset_collate;";

    dbDelta( $sql_orders );


    $tickets_table_name = $wpdb->prefix . "boletia_tickets";
    $sql_tickets = "CREATE TABLE $tickets_table_name (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    ticket_id VARCHAR(50) NULL,
    order_id VARCHAR(50) NULL,
    user_id VARCHAR(50) NULL,
    type VARCHAR(50) NULL,
    ticket_type_name VARCHAR(255) NULL,
    consecutive_number int(10) NULL,
    sku VARCHAR(255) NULL,
    first_name VARCHAR(255) NULL,
    last_name VARCHAR(255) NULL,
    email VARCHAR(255) NULL,
    created VARCHAR(255) NULL,
    updated VARCHAR(255) NULL,
    PRIMARY KEY  (id)
    ) $charset_collate;";

    dbDelta( $sql_tickets );
    
}