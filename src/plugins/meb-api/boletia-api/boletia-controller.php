<?php

class MEB_Boletia_Controller extends WP_REST_Controller
{

    public function register_routes()
    {
        $namespace = 'meb/v1';
        $path = 'boletia';

        register_rest_route($namespace, '/' . $path . '/orders', [
            array(
                'methods'             => 'POST',
                'callback'            => array($this, 'update_item'),
                'permission_callback' => array($this, 'update_item_permissions_check')
            ),

        ]);
    }

    public function update_item_permissions_check($request)
    {
        return current_user_can('edit_others_posts');
    }

    public function update_item($request)
    {
        $params = $request->get_json_params();
        global $wpdb;
        $orders_table = $wpdb->prefix . 'boletia_orders';
        $success = 0;
        $now = date("Y-m-d H:i:s");

        $order_id = $params['data'][0]['id'];
        $order = $wpdb->get_row("SELECT * FROM $orders_table WHERE order_id = \"$order_id\"");
        $order_data = array(
            'order_id' => $order_id,
            'type' => $params['data'][0]['type'],
            'status' => $params['data'][0]['attributes']['status'],
            'created_at' => $params['data'][0]['attributes']['created_at'],
            'relationships' => serialize($params['data'][0]['relationships']),
            'raw_json' => json_encode($params),
            'created' => $now,
            'updated' => $now,
        );
        $order_data_types = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');
        if (!$order) {
            $success = $wpdb->insert(
                $orders_table,
                $order_data,
                $order_data_types
            );
        } else {
            unset($order_data['created']);
            unset($order_data_types[6]);
            $success = $wpdb->update(
                $orders_table,
                $order_data,
                array('id' => $order->id),
                $order_data_types,
                array('%d')
            );
        }

        $tickets_table = $wpdb->prefix . 'boletia_tickets';
        foreach ($params['included'] as $ticket) {
            $ticket_id = $ticket['id'];

            $ticket_row = $wpdb->get_row("SELECT * FROM $tickets_table WHERE ticket_id = \"$ticket_id\"");
            $ticket_data = array(
                'ticket_id' => $ticket_id,
                'order_id' => $order_id,
                'type' => $ticket['type'],
                'ticket_type_name' => $ticket['attributes']['ticket_type_name'],
                'consecutive_number' => $ticket['attributes']['consecutive_number'],
                'sku' => $ticket['attributes']['sku'],
                'first_name' => $ticket['attributes']['first_name'],
                'last_name' => $ticket['attributes']['last_name'],
                'email' => $ticket['attributes']['email'],
                'created' => $now,
                'updated' => $now,
            );

            $ticket_data_types = array('%s', '%s', '%s', '%s', '%d', '%s', '%s', '%s', '%s', '%s', '%s');
            if (!$ticket_row) {
                $success = $wpdb->insert(
                    $tickets_table,
                    $ticket_data,
                    $ticket_data_types
                );
            } else {
                unset($ticket_data['created']);
                unset($ticket_data_types[8]);
                $success = $wpdb->update(
                    $tickets_table,
                    $ticket_data,
                    array('id' => $ticket_row->id),
                    $ticket_data_types,
                    array('%d')
                );
            }
        }

        $response = array();
        if($success){
            $response = new WP_REST_Response(array(
                'code' => 'boletia_order_success',
                'message' => 'La orden ha sido dada de alta exitosamente',
                'data' => array(
                    'status' => 200
                )
            ));
            $response->set_status(200);
        }else{
            $response = new WP_REST_Response(array(
                'code' => 'boletia_order_error',
                'message' => 'Ha habido un problema al dar de alta la orden',
                'data' => array(
                    'status' => 500,
                    'error' => 'Database error'
                )
            ));
            $response->set_status(500);
        }

        return $response;
    }
}
