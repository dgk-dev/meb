<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'meb_container_type' );
?>

<div class="wrapper" id="archive-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php
				if ( have_posts() ) {
					?>
					<header class="page-header">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 offset-lg-1">
                               <h1 class="page-title">Experiencia MEB</h1>
                            </div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-lg-7 offset-lg-1">
								<div style="font-size: 1.5rem">
									Revive momentos especiales de ediciones previas del Festival Internacional de Gastronomía y Vino “Morelia en Boca”.
								</div>
							</div>
						</div>
					</header><!-- .page-header -->
					<div class="row justify-content-center">
						<div class="col-sm-12 col-lg-10">
							<div class="row">
								<?php while ( have_posts() ): the_post(); ?>
									<div class="col-sm-12 col-lg-6">
										<div class="meb-experience-item">
											<div class="row">
												<div class="col-sm-12 col-lg-7">
													<div class="meb-experience-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>');"></div>
												</div>
												<div class="col-sm-12 col-lg-5">
													<div class="meb-experience-name">
														<?php echo get_the_title(); ?>
													</div>
													<div class="meb-experience-date">
														<?php echo get_post_meta(get_the_ID(), 'meb-experience-date', true); ?>
													</div>
													<a href="<?php echo get_the_permalink(); ?>" class="btn btn-outline-primary">Ver galería</a>
												</div>
											</div>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
					
                <?php
				} else {
					get_template_part( 'loop-templates/content', 'none' );
				}
				?>

			</main><!-- #main -->

			<?php
			// Display the pagination component.
			meb_pagination();
			// Do the right sidebar check.
			get_template_part( 'global-templates/right-sidebar-check' );
			?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #archive-wrapper -->

<?php
get_footer();
