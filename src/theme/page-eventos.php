<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('meb_container_type');

?>

<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row justify-content-lg-center">
            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">

                <article <?php post_class(); ?> id="events-list">

                    <header class="entry-header">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 offset-lg-1">
                                <h1 class="entry-title">Eventos</h1>
                            </div>
                        </div>
                        <div class="row justify-content-lg-center">
                            <div class="col-sm-12 col-lg-10">
                                <div class="venue-selection">
                                    <?php
                                    $tribe_venues = tribe_get_venues();
                                    $active_venue = '';
                                    $active_name = '';
                                    if ($tribe_venues) :
                                        if ($_GET['venue_id']) {
                                            foreach ($tribe_venues as $venue) {
                                                if ($venue->ID == $_GET['venue_id']) {
                                                    $active_venue = $venue->ID;
                                                    $active_name = $venue->post_title;
                                                    break;
                                                }
                                            }
                                        }
                                        $active_name = $active_name ? $active_name : $tribe_venues[0]->post_title;
                                        $active_venue = $active_venue ? $active_venue : $tribe_venues[0]->ID;
                                    ?>
                                        <span class="select-title">Sede:</span>
                                        <div class="dropdown" id="venue-select">
                                            <a href="#" class="btn dropdown-toggle" ref="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="selected-title"><?php echo $active_name; ?></span> <i class="fas fa-chevron-down"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" data-boundary="viewport">
                                                <?php foreach ($tribe_venues as $venue) : ?>
                                                    <li class="dropdown-item">
                                                        <a class="dropdown-item <?php echo $active_venue == $venue->ID ? 'active' : '' ?>" href="#" data-venue-id="<?php echo $venue->ID ?>"><?php echo $venue->post_title ?></a>
                                                    </li>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </header><!-- .entry-header -->

                    <div class="entry-content" id="events-list-container" data-venue-id="<?php echo $active_venue ?>">

                    </div><!-- .entry-content -->
                    <div class="row">
                        <div class="col text-center">
                            <?php meb_loader(); ?>
                        </div>
                    </div>

                </article><!-- #post-## -->

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
