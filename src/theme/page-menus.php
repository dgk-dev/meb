<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('meb_container_type');

?>

<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">
            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">

                <article <?php post_class(); ?> id="menu-list">

                    <header class="entry-header">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 offset-lg-1">
                                <h1 class="entry-title">Menús</h1>
                            </div>
                        </div>
                    </header><!-- .entry-header -->
                    <div class="entry-content">
                        <?php
                            $request = new WP_REST_Request( 'GET', '/meb/v1/events/menus' );
                            $response = rest_do_request( $request );
                            $server = rest_get_server();
                            $menus = $response->status == 200 ? $server->response_to_data( $response, false ) : array();

                            $events_menus = array();
                            $day_menus = array();
                            foreach($menus as $menu){
                                foreach($menu['related_events'] as $event){
                                    if(in_array('Menú', $event['categories']) && !in_array($menu['id'], $day_menus)){
                                        $day_menus[$menu['id']]= $menu;
                                    }elseif(!in_array($menu['id'], $events_menus)){
                                        $events_menus[$menu['id']]= $menu;
                                    }
                                }
                            }
                        
                        ?>
                        <?php if(!empty($day_menus)): ?>
                            <div class="row pb-4">
                                <div class="col-sm-12 col-lg-5 offset-lg-1">
                                    <div style="font-size: 1.5rem">
                                        Disfruta de los deliciosos platillos del 09 al 15 de noviembre en nuestras sedes disponibles
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-lg-center pb-4">
                                <div class="col-sm-12 col-lg-10">
                                    <div class="row" id="menu-list-container">
                                        <?php foreach($day_menus as $day_menu): ?>
                                            <div class="col-sm-12 col-lg-5 offset-lg-1">
                                                <div class="meb-menu-item">
                                                    <div class="meb-menu-name">
                                                        <a href="<?php echo $day_menu['permalink'] ?>"><?php echo $day_menu['name'] ?></a>
                                                    </div>
                                                    <div class="meb-menu-guests">
                                                    <?php
                                                            $venue_guests = array();
                                                            foreach($day_menu['related_events'] as $event) {
                                                               
                                                                if(!in_array($event['venue']['id'], $venue_guests)){
                                                                    $venue_guests[$event['venue']['id']] = $event['venue'];
                                                                    $venue_guests[$event['venue']['id']]['total_guests'] = array();
                                                                }
                                                                
                                                                foreach($event['guests'] as $guest){
                                                                    if(!in_array($guest['id'], $venue_guests[$event['venue']['id']]['total_guests']))
                                                                        $venue_guests[$event['venue']['id']]['total_guests'][$guest['id']]= $guest;  
                                                                }
                                                            }
                                                        ?>
                                                        
                                                        <?php foreach($venue_guests as $venue): ?>
                                                            <div class="guest">
                                                                <?php foreach($venue['total_guests'] as $guest): ?>
                                                                    Invitado: <a href="<?php echo $guest['permalink'] ?>"><?php echo $guest['name'] ?></a> <br>
                                                                <?php endforeach; ?>
                                                                Restaurante: <a href="<?php echo $venue['permalink']; ?>"><?php echo $venue['name']; ?></a> <br>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if(!empty($events_menus)): ?>
                            <div class="row pb-4">
                                <div class="col-sm-12 col-lg-4 offset-lg-1">
                                    <div style="font-size: 1.5rem">
                                        Disfruta los siguientes menús en nuestros eventos especiales para ti 
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-lg-center pb-4">
                                <div class="col-sm-12 col-lg-10">
                                    <div class="row" id="menu-list-container">
                                        <?php foreach($events_menus as $event_menu): ?>
                                            <div class="col-sm-12 col-lg-5 offset-lg-1">
                                                <div class="meb-menu-item">
                                                    <div class="meb-menu-name">
                                                        <a href="<?php echo $event_menu['permalink'] ?>"><?php echo $event_menu['name'] ?></a>
                                                    </div>
                                                    <div class="meb-menu-guests">
                                                        <?php
                                                            $venue_guests = array();
                                                            foreach($event_menu['related_events'] as $event) {
                                                               
                                                                if(!in_array($event['venue']['id'], $venue_guests)){
                                                                    $venue_guests[$event['venue']['id']] = $event['venue'];
                                                                    $venue_guests[$event['venue']['id']]['total_guests'] = array();
                                                                }
                                                                
                                                                foreach($event['guests'] as $guest){
                                                                    if(!in_array($guest['id'], $venue_guests[$event['venue']['id']]['total_guests']))
                                                                        $venue_guests[$event['venue']['id']]['total_guests'][$guest['id']]= $guest;  
                                                                }
                                                            }
                                                        ?>
                                                        
                                                        <?php foreach($venue_guests as $venue): ?>
                                                            <div class="guest">
                                                                <?php foreach($venue['total_guests'] as $guest): ?>
                                                                    Invitado: <a href="<?php echo $guest['permalink'] ?>"><?php echo $guest['name'] ?></a> <br>
                                                                <?php endforeach; ?>
                                                                Restaurante: <a href="<?php echo $venue['permalink']; ?>"><?php echo $venue['name']; ?></a> <br>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
