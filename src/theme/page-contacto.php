<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('meb_container_type');

?>

<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">
            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">
                <?php while ( have_posts() ): the_post(); ?>
                    <article <?php post_class(); ?> id="menu-list">

                        <header class="entry-header">
                            <div class="row">
                                <div class="col-sm-12 col-lg-4 offset-lg-1">
				                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                                </div>
                            </div>
                        </header><!-- .entry-header -->
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-12 col-lg-5 offset-lg-1">
                                    <div class="contact-address pb-4">
                                        Avenida Morelos Norte No. 485. Centro Histórico. Morelia, Michoacán.
                                    </div>
                                    <div class="contact-info">
                                        <div class="pb-2">
                                            <a href="mailto: info@moreliaenboca.com" target="_BLANK"><img src="<?php echo get_template_directory_uri()?>/img/icon-mail.svg" alt="icon mail"> info@moreliaenboca.com</a> <br>

                                        </div>
                                        <div>
                                            <img src="<?php echo get_template_directory_uri()?>/img/icon-phone.svg" alt=" icon phone"> +52: <span style="color: #cf1143;">(55)</span> 521 19976, <span style="color: #cf1143">(443)</span> 313 9253
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4 offset-lg-1">
                                    <?php the_content(); ?>
                                </div>
                            </div>

                        </div><!-- .entry-content -->

                    </article><!-- #post-## -->
                <?php endwhile; ?>
            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
