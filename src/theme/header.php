<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'meb_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/safari-pinned-tab.svg" color="#3a3a3a">
	<meta name="msapplication-TileColor" content="#fff9ee">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php meb_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav id="main-nav" class="navbar navbar-expand-xl navbar-light" aria-labelledby="main-nav-label">

			<h2 id="main-nav-label" class="sr-only">
				<?php esc_html_e( 'Main Navigation', 'understrap' ); ?>
			</h2>

		<?php if ( 'container' === $container ) : ?>
			<div class="container">
		<?php endif; ?>
				<button class="navbar-toggler" type="button" data-toggle="offcanvas" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'navbar-collapse offcanvas-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav mr-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				);
				?>

				<ul id="user-menu" class="navbar-nav ml-auto">
					<?php if(is_user_logged_in()): ?>
						<li class="user-item">
							<a class="nav-link btn btn-outline-primary" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Mi cuenta</a>
						</li>
					<?php else: ?>
						<li class="user-item">
							<a class="nav-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Login</a>
						</li>
						<li class="user-item">
							<a class="nav-link btn btn-outline-primary" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#sign-up">Sign up</a>
						</li>
					<?php endif; ?>
				</ul>
				<ul id="social-menu" class="navbar-nav d-none d-lg-flex">
					<li class="social-item">
						<a href="https://www.facebook.com/moreliaenboca" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook"></i></a>
					</li>
					<li class="social-item">
						<a href="https://www.instagram.com/meboficial/" target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram"></i></a>
					</li>
					<li class="social-item">
						<a href="https://twitter.com/moreliaenboca" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
					</li>
				</ul>
				<div class="d-flex">
					<?php if (function_exists('meb_cart')) meb_cart() ?>
				</div>

				<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><img src="<?php echo get_template_directory_uri(). '/img/1-e-logo-meb.svg' ?>" alt=""></a>

			<?php if ( 'container' === $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
