<?php

/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if (!defined('ABSPATH')) {
	die('-1');
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();
$container = get_theme_mod('meb_container_type');
?>
<div class="wrapper py-4" id="page-wrapper">
	<div id="tribe-events-content" class="tribe-events-single <?php echo esc_attr($container); ?>">

		<?php while (have_posts()) :  the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row">
					<div class="col-sm-12 col-lg-5 offset-lg-1">
						<!-- Event featured image, but exclude link -->
						<?php echo tribe_event_featured_image($event_id, 'full', false); ?>
					</div>
					<div class="col-sm-12 col-lg-5">
						<?php the_title('<h1 class="tribe-events-single-event-title">', '</h1>'); ?>
						<!-- Event content -->
						<?php //do_action('tribe_events_single_event_before_the_content') ?>
						<div class="tribe-events-single-event-description tribe-events-content">
							<?php the_content(); ?>
						</div>
						<!-- .tribe-events-single-event-description -->
						<?php //do_action('tribe_events_single_event_after_the_content') ?>
					</div>
				</div>
				<!-- Event meta -->
				<?php do_action('tribe_events_single_event_before_the_meta') ?>
				<?php tribe_get_template_part('modules/meta'); ?>
				<div class="row justify-content-center">
					<div class="col-sm-12 col-lg-10" id="comprar">
						<?php do_action('tribe_events_single_event_after_the_meta') ?>
					</div>
				</div>
			</div> <!-- #post-x -->
			<?php if (get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option('showComments', false)) comments_template() ?>
		<?php endwhile; ?>

	</div><!-- #tribe-events-content -->
</div>