<?php

/**
 * Single Menu Template
 * The template for an menu. It displays menu information and lists
 * events that occur with the specified menu.
 *
 * This view is based on /wp-content/plugins/events-calendar-pro/src/views/pro/single-organizer.php
 *
 * This template can be copied to [your-active-child-theme]/tribe-events/tribe_ext_menu/single.php and then customized
 */

// Do not allow loading directly or in an unexpected manner.
if (!class_exists('Tribe__Extension__Guest_Linked_Post_Type')) {
    return;
}

global $post;

$request = new WP_REST_Request( 'GET', '/meb/v1/events/guests/'.$post->ID );
$response = rest_do_request( $request );
$server = rest_get_server();
$guest = $response->status == 200 ? $server->response_to_data( $response, false ) : array();

$container = get_theme_mod('meb_container_type');
?>
<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">
                <div class="row pb-4">
                    <div class="col-sm-12 col-lg-5 order-lg-2">
                        <?php meb_post_navigation(); ?>
                        <h1 class="entry-title-guest-name"><?php echo $guest['name']; ?></h1>
                        <?php echo $guest['description']; ?>
                        <div class="pt-4">
                            <?php meb_post_navigation(); ?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-5 order-lg-1 offset-lg-1 col-guest-image">
                        <img class="img-fluid pb-4" src="<?php echo $guest['image']; ?>" alt="<?php echo $guest['name']; ?> img">
                        <?php if(!empty($guest['related_events'])): ?>
                            <h2 class="entry-subtitle py-4">Eventos vinculados</h2>
                            <div class="row pb-4">
                                <?php
                                    
                                    $date_format = 'd-m-Y H:i:s';
                                    foreach($guest['related_events'] as $event):
                                        $start_date_obj = DateTime::createFromFormat($date_format, $event['start_date']);
                                        $end_date_obj = DateTime::createFromFormat($date_format, $event['end_date']);
                                ?>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="related-event">
                                            <div class="related-event-day">
                                                <?php echo date_i18n('d', $start_date_obj->getTimestamp()).'/'.date_i18n('F', $start_date_obj->getTimestamp());?>
                                            </div>
                                            <div class="related-event-hour">
                                                <?php echo $event['date_data']['start_date']['hour'].' - '.$event['date_data']['end_date']['hour'] ?>
                                            </div>
                                            <div class="related-event-name">
                                                <a href="<?php echo $event['permalink'];?>"><?php echo $event['name'];?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <?php if(!empty($guest['related_menus'])): ?>
                            <h2 class="entry-subtitle pb-4">Menús</h2>
                            <div class="row">
                                <?php foreach($guest['related_menus'] as $menu): ?>
                                    <div class="col-sm-12 col-lg-6">
                                        <div class="related-menu">
                                            <div class="related-menu-name">
                                                <a href="<?php echo $menu['permalink'] ?>"><?php echo $menu['name']; ?></a>
                                            </div>
                                            <?php foreach($menu['venues'] as $venue): ?>
                                                <div class="related-menu-venue">
                                                    <a href="<?php echo $venue['permalink'] ?>"><?php echo $venue['name'] ?></a>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?> 
                    </div>
                </div>
            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->