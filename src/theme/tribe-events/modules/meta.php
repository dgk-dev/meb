<?php
/**
 * Single Event Meta Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta.php
 *
 * @version 4.6.10
 *
 * @package TribeEventsCalendar
 */

do_action( 'tribe_events_single_meta_before' );
?>

<?php do_action( 'tribe_events_single_event_meta_primary_section_start' ); ?>
<div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
    <div class="row py-4">
        <div class="col-sm-12 col-lg-3 offset-lg-1">
            <?php
            // Always include the main event details in this first section
            tribe_get_template_part( 'modules/meta/details' );
            ?>
        </div>
        <div class="col-sm-12 col-lg-3">
            <?php
            tribe_get_template_part( 'modules/meta/venue' );
            ?>
        </div>
        <div class="col-sm-12 col-lg-4">
        <?php
            tribe_get_template_part( 'modules/meta/map' );
            ?>
        </div>
    </div>
</div>
<?php do_action( 'tribe_events_single_event_meta_primary_section_end' ); ?>

<?php do_action( 'tribe_events_single_meta_after' );
