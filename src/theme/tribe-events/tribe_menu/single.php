<?php

/**
 * Single Menu Template
 * The template for an menu. It displays menu information and lists
 * events that occur with the specified menu.
 *
 * This view is based on /wp-content/plugins/events-calendar-pro/src/views/pro/single-organizer.php
 *
 * This template can be copied to [your-active-child-theme]/tribe-events/tribe_ext_menu/single.php and then customized
 */

// Do not allow loading directly or in an unexpected manner.
if (!class_exists('Tribe__Extension__Menu_Linked_Post_Type')) {
    return;
}

global $post;

$request = new WP_REST_Request( 'GET', '/meb/v1/events/menus/'.$post->ID );
$response = rest_do_request( $request );
$server = rest_get_server();
$menu = $response->status == 200 ? $server->response_to_data( $response, false ) : array();

$container = get_theme_mod('meb_container_type');
?>
<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">
                <div class="row pb-4">
                    <div class="col-sm-12 col-lg-5 offset-lg-1">
                        <?php meb_post_navigation(); ?>
                        <h1 class="entry-title"><?php echo $menu['name']; ?></h1>
                        <?php echo $menu['description']; ?>

                    </div>
                    <div class="col-sm-12 col-lg-5 col-menu-image">
                        <img class="img-fluid" src="<?php echo $menu['image']; ?>" alt="<?php echo $menu['name']; ?> img">
                    </div>
                </div>
                <?php if(!empty($menu['related_events'])): ?>
                    <div class="row pb-4">
                        <div class="col-sm-12 col-lg-auto offset-lg-1">
                            <h2 class="entry-subtitle">Eventos vinculados</h2>
                        </div>
                    </div>
                    
                    <div class="row pb-4">
                        <?php
                        
                            $date_format = 'd-m-Y H:i:s';
                            
                            foreach($menu['related_events'] as $event):
                                $start_date_obj = DateTime::createFromFormat($date_format, $event['start_date']);
                                $end_date_obj = DateTime::createFromFormat($date_format, $event['end_date']);
                                
                        ?>
                            <div class="col-sm-12 col-lg-auto offset-lg-1">
                                <div class="related-event">
                                    <div class="related-event-day">
                                        <?php echo date_i18n('d', $start_date_obj->getTimestamp()).'/'.date_i18n('F', $start_date_obj->getTimestamp());?>
                                    </div>
                                    <div class="related-event-hour">
                                        <?php echo $event['date_data']['start_date']['hour'].' - '.$event['date_data']['end_date']['hour'] ?>
                                    </div>
                                    <div class="related-event-name">
                                        <a href="<?php echo $event['permalink'];?>"><?php echo $event['name'];?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($menu['related_venues'])): ?>
                    <div class="row pb-1">
                        <div class="col-sm-12 col-lg-auto offset-lg-1">
                            <h2 class="entry-subtitle">Sedes</h2>
                        </div>
                    </div>
                    
                    <div class="row pb-4">
                        <?php foreach($menu['related_venues'] as $venue): ?>
                            <div class="col-sm-12 col-lg-auto offset-lg-1">
                                <div class="related-venue">
                                    <a href="<?php echo $venue['permalink'] ?>"><?php echo $venue['name'] ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($menu['related_guests'])): ?>

                    <div class="row pb-4">
                        <div class="col-sm-12 col-lg-auto offset-lg-1">
                            <h2 class="entry-subtitle">Invitados</h2>
                        </div>
                    </div>
                    <div class="row pb-4">
                        <?php foreach($menu['related_guests'] as $guest): ?>
                            <div class="col-sm-12 col-lg-5 offset-lg-1">
                                <div class="related-guest">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="guest-img" style="background-image: url('<?php echo $guest['image']; ?>');"></div>
                                        </div>
                                        <div class="col">
                                            <div class="guest-name">
                                                <?php echo $guest['name']; ?>
                                            </div>
                                            <div class="guest-data">
                                            <?php echo $guest['origin']; ?>
                                            </div>
                                            <a href="<?php echo $guest['permalink']; ?>" class="btn btn-outline-primary">Ver perfil</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if(!empty($menu['video'])):
                        $videos = explode('|', $menu['video']);
                        foreach($videos as $video):
                ?>
                            <div class="row justify-content-center pb-4">
                                <?php $embed_url = meb_get_youtube_embed_url($video); ?>
                                <div class="col-sm-12 col-md-10 col-lg-8">
                                    <div class="menu-video">
                                    <iframe
                                        width="459"
                                        height="344"
                                        src="<?php echo $embed_url ?>"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    ></iframe>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                    endif; ?>
            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->