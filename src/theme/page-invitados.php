<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('meb_container_type');

?>

<div class="wrapper py-4" id="page-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">
            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">

                <article <?php post_class(); ?> id="guest-list">

                    <header class="entry-header">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 offset-lg-1">
                                <h1 class="entry-title">Invitados</h1>
                                <div style="font-size: 1.5rem">
                                    Conoce a nuestros invitados y su participación en MEB 2020.
                                </div>
                            </div>
                        </div>
                    </header><!-- .entry-header -->

                    <div class="entry-content pt-5">
                        <div class="row justify-content-lg-center">
                            <div class="col-sm-12 col-lg-10">
                                <?php
                                    $request = new WP_REST_Request( 'GET', '/meb/v1/events/guests' );
                                    $response = rest_do_request( $request );
                                    $server = rest_get_server();
                                    $guests = $response->status == 200 ? $server->response_to_data( $response, false ) : array();
                                
                                ?>
                                
                                <div class="row" id="guest-list-container">
                                    <?php foreach($guests as $guest): ?>
                                        <div class="col-sm-12 col-lg-5 offset-lg-1">
                                            <div class="guest-item">
                                                <div class="row">
                                                    <div class="col-auto">
                                                        <div class="guest-img" style="background-image: url('<?php echo $guest['image']; ?>');"></div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="guest-name">
                                                            <?php echo $guest['name']; ?>
                                                        </div>
                                                        <div class="guest-data">
                                                            <?php echo $guest['origin']; ?>
                                                        </div>
                                                        <a href="<?php echo $guest['permalink']; ?>" class="btn btn-outline-primary">Ver perfil</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
