<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'meb_container_type' );

?>

<div class="wrapper py-4" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">
            <!-- Do the left sidebar check -->
            <?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

            <main class="site-main" id="main">

                <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                    <header class="entry-header">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 offset-lg-1">
                                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                            </div>
                        </div>
                    </header><!-- .entry-header -->

                    <div class="entry-content">
                        <div class="row align-items-top justify-content-md-center" id="meb-presentation-wrapperr">
                            <div class="col-sm-12 col-lg-5">
                                <img src="<?php echo get_template_directory_uri().'/img/1-e-logo-meb.svg' ?>" class="img-fluid" alt="Morelia en Boca" style="margin-bottom: 1.6rem; width: 350px;">
                            </div>
                            
                            <div class="col-sm-12 col-lg-5">
                                <p><span class="strong-text">"Morelia en Boca",</span> El Festival Internacional de Gastronomía y Vino de Morelia, cumple este 2020 sus primeros 10 años ofreciendo un espacio a la creatividad, a la gastronomía, a los productos del campo y al vino mexicano, en uno de los estados más bellos de nuestro país. </p>
                                
                                <p>Nuevamente Michoacán, y la ciudad de Morelia, Patrimonio Cultural de la Humanidad y sede de diversas expresiones artísticas, se complace en presentar la décima edición de un festival que se ha consolidado integrando en una plataforma a cocineras tradicionales, a chefs, a productores agroalimentarios y enólogos, a estudiantes y demás partícipes de la industria gastronómica.</p>

                                <p>Morelia en Boca ha logrado a través de los años, de su contenido y de su propuesta gastronómica, llamar la atención y canalizar la mirada del mundo en Michoacán.</p>

                                <p>En un momento de adversidades debido a la pandemia, nos hemos transformado en ésta edición, en un festival híbrido, convirtiéndonos en el primer festival gastronómico de México con dicho formato y, si bien ha sido un gran reto, esto solo demuestra la fuerza de Michoacán y su gastronomía.</p>

                                <p>Hace 10 años, y gracias al paradigma Michoacán, la UNESCO reconoció a la Gastronomía Mexicana como patrimonio intangible de la humanidad, y hoy Morelia en Boca se viste de gala, y a la distancia, y en pequeños espacios, y sin tumulto, pero con mucho ruido, para celebrar dicho reconocimiento y continuar siendo testigo, poniendo en alto la gastronomía de nuestro estado y del país en todo el mundo.</p>

                                <p>Una fiesta más grande que otros años, que tendrá presencia en diversos puntos de la república Mexicana, con esfuerzos y colaboraciones que anclan aún más la amistad con todos quienes participamos en esta edición tan especial.</p>

                                <p>Por primera vez <strong>"Llevamos Morelia en Boca, a todo México"</strong> y le damos la más cordial bienvenida al público nacional y extranjero, a los comensales, estudiantes, chefs, productores, enólogos, cocineras tradicionales, patrocinadores y a todos quienes buscamos en la gastronomía de nuestro país lo que durante años nos ha distinguido, como un territorio de riqueza cultural, turística y gastronómica.</p>

                                <p>Sean todos bienvenidos a la décima edición del Festival Internacional de Gastronomía y Vino de Morelia, que abre las puertas de sus más de 24 sedes, para recibirlos como solo los mejores invitados se merecen.</p>

                                <p>
                                    <strong>Fernando Pérez Vera</strong><br>
                                    <strong>Director</strong><br>
                                    <strong>Morelia en Boca</strong>
                                </p>

                            </div>
                        </div>
                        <div class="row pt-4 justify-content-lg-center">
                            <div class="col-auto">
                                <p class="strong-text text-center">
                                    <strong><u>Directorio MEB 2020</u></strong>
                                </p>

                                <p>
                                    <strong>Dirección</strong><br>
                                    Fernando Pérez / Director <br>
                                    Fernando Figueroa/ Director Adjunto 
                                </p>

                                <p>
                                    <strong>Coordinación General</strong><br>
                                    Nadia Anaya / Coordinadora General 
                                </p>

                                <p>
                                    <strong>Administración y Contabilidad</strong><br>
                                    Rodolfo Romero / Contador <br>
                                    Eugenia Juárez / Auxiliar Administrativo 
                                </p>

                                <p>
                                    <strong>Relaciones Públicas y Prensa</strong><br>
                                    Jaqueline Benítez / Coordinadora de Relaciones Públicas <br>
                                    Alma González / Asistente de Relaciones Públicas y Redes Sociales <br>
                                    Alejandra Leñero/ Asistente de Relaciones Públicas y Prensa especializada
                                </p>

                                <p>
                                    <strong>Comercialización</strong> <br>
                                    Ana C. Gómez / Coordinadora Comercial <br>
                                    Airam Piñón Serrano/ Asistente de Comercialización
                                </p>

                                <p>
                                    <strong>Imagen y Material Gráfico</strong><br>
                                    Fernando Llanos / Idea original de la décima edición <br>
                                    Patricia Anguiano / Diseñadora Gráfica
                                </p>

                                <p>
                                    <strong>Gastronomía</strong><br>
                                    Sergio Camacho/ Coordinador Gastronómico <br>
                                    Enrique Farjeat/ Asesor Gastronómico <br>
                                    Paulina Téllez/ Operación Gastronómica <br>
                                    Karina Alanís/ Coordinadora del programa de Cocina Tradicional
                                </p>

                                <p>
                                    <strong>Vinos y bebidas</strong><br>
                                    Ana Arreola/ Coordinadora de Vinos y Bebidas
                                </p>

                                <p>
                                    <strong>Productores de Michoacán</strong><br>
                                    Josefina Orozco / Coordinadora del programa de productores
                                </p> 

                                <p>
                                    <strong>Página WEB</strong><br>
                                    DGK Agencia de Marketing Digital
                                </p>
                            </div>
                        </div>
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

        </div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();