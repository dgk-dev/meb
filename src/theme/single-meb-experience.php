<?php

/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$container = get_theme_mod('meb_container_type');
?>

<div class="wrapper" id="single-wrapper">

    <div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

        <div class="row">

            <!-- Do the left sidebar check -->
            <?php get_template_part('global-templates/left-sidebar-check'); ?>

            <main class="site-main" id="main">

                <?php while (have_posts()) : the_post(); ?>
                    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                     
                    
                        <header class="entry-header">
                            <div class="row">
                                <div class="col-sm-12 col-lg-10 offset-lg-1">
                                    <div class="back-navigation">
                                        <a href="<?php echo get_post_type_archive_link('meb-experience'); ?>"><span class="back-arrow"></span> Experiencia MEB</a>
                                    </div>
                                </div>
                            </div>
                        </header><!-- .entry-header -->

                        
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-12 col-lg-3 offset-lg-1 pb-4">
                                    <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
                                </div>
                                <div class="col-sm-12 col-lg-7">
                                    <?php the_title('<h1 class="meb-experience-title pb-4">', '</h1>'); ?>
                                    <div class="meb-experience-content" data-date="<?php echo get_post_meta($post->ID, 'meb-experience-date', true); ?>">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            


                        </div><!-- .entry-content -->

                        <footer class="entry-footer">
                            <div class="row justify-content-center">
                                <div class="col-sm-8 col-lg-6">
                                    <div class="meb-experience-gallery pt-4">
                                        
                                    </div>
                                </div>
                            </div>
                        </footer><!-- .entry-footer -->

                    </article><!-- #post-## -->


                <?php endwhile; ?>

            </main><!-- #main -->

            <!-- Do the right sidebar check -->
            <?php get_template_part('global-templates/right-sidebar-check'); ?>

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #single-wrapper -->
<script>
    (function ($) {
        $(window).load(function(){
            var $content = $('.meb-experience-content');
            var prependtxt = $content.attr('data-date').length ? '<span class="meb-experience-inline-date">'+$content.attr('data-date')+'.</span> ' : '';
            $('.meb-experience-content p').first().prepend(prependtxt);
        })
    })(jQuery);
</script>
<?php
get_footer();
