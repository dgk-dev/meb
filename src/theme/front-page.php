<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'meb_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
        <div class="row pb-4">
            <div class="col-sm-12 content-area" id="primary">
                <main class="site-main" id="main">
        
                    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
        
                        <div class="entry-content">
                            <div id="mebCarousel" class="carousel slide" data-ride="carousel" data-interval="7000">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <a href="/evento/cena-maridaje-presencial-en-hotel-casa-grande/"><img class="d-block d-lg-none d-xl-none w-100" src="<?php echo get_template_directory_uri() ?>/img/banner-cena_778x820.jpg" alt="Evento Cena Maridaje banner"></a>
                                        <a href="/evento/cena-maridaje-presencial-en-hotel-casa-grande/"><img class="d-none d-lg-block d-xl-block w-100" src="<?php echo get_template_directory_uri() ?>/img/banner-cena_1110x760.jpg" alt="Evento Cena Maridaje banner"></a>
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block d-lg-none d-xl-none w-100" src="<?php echo get_template_directory_uri() ?>/img/10-mb_778x820.png" alt="10 MEB banner">
                                        <img class="d-none d-lg-block d-xl-block w-100" src="<?php echo get_template_directory_uri() ?>/img/v2-10-mb_1110x760.jpg" alt="10 MEB banner">
                                    </div>
                                    
                                </div>
                                <a class="carousel-control-prev" href="#mebCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                                <a class="carousel-control-next" href="#mebCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Siguiente</span>
                                </a>
                            </div>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
        
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .row -->
        

    </div><!-- #content -->
    
    <div class="row-fluid py-4" id="eventos">
        <div id="meb-map"></div>
    </div>

    <div class="container-fluid" id="patrocinadores">
        <div class="row pt-4">
            <div class="col-md-12 text-center">
                <div class="sponsors">
                    <a href="https://www.piaceremexico.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-pieacere.svg" alt="logo-pieacere" class="sponsor"></a>
                    <a href="https://www.jcdecauxlatam.com/mx" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-jc-decaux.svg" alt="logo-jc-decaux" class="sponsor"></a>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-garcicrespo.svg" alt="logo-garcicrespo" class="sponsor">
                    <a href="https://aeromexico.com/es-mx" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-aeromexico.svg" alt="logo-aeromexico" class="sponsor"></a>
                    <a href="https://www.hoteljuaninos.com.mx/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-los-juaninos.svg" alt="logo-los-juaninos" class="sponsor"></a>
                    <a href="https://www.facebook.com/LosMirasoles" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-los-mirasoles.svg" alt="logo-los-mirasoles" class="sponsor"></a>
                    <a href="https://www.facebook.com/LBistrot/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-l-bistrot.svg" alt="logo-l-bistrot" class="sponsor"></a>
                    <a href="https://comepesca.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-comepesca.svg" alt="logo-comepesca" class="sponsor"></a>
                    <a href="https://pescaconfuturo.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-pesca-con-futuro.svg" alt="logo-pesca-con-futuro" class="sponsor"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="sponsors">
                    <a href="http://www.experienciamorelia.mx/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-morelia@2x.png" alt="logo-morelia" class="sponsor"></a>
                    <a href="https://www.morelia.gob.mx" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-morelia-h-ayuntamiento.svg" alt="logo-morelia-h-ayuntamiento" class="sponsor"></a>
                    <a href="http://michoacan.travel/es/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-michoacan-celebra.svg" alt="logo-michoacan-celebra" class="sponsor"></a>
                    <a href="https://michoacan.gob.mx/" target="_BLANK"><img style="margin-right: 0;" src="<?php echo get_template_directory_uri(); ?>/img/logo-gobierno-michoacan.svg" alt="logo-gobierno-michoacan" class="sponsor"></a>
                    <a href="https://michoacan.gob.mx/michoacan-se-escucha/" target="_BLANK"><img style="margin-left: 0;" src="<?php echo get_template_directory_uri(); ?>/img/logo-gobierno-michoacan-escucha.svg" alt="logo-gobierno-michoacan" class="sponsor"></a>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-fifopratur.svg" alt="logo-fifopratur" class="sponsor">
                    <a href="http://sedrua.michoacan.gob.mx/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-sedagro@2x.png" alt="logo-sedagro" class="sponsor"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-marcas-asociadas.svg" alt="logo-marcas-asociadas">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="associated-brands">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-mafalda.svg" alt="logo-mafalda" class="brand">
                    <a href="https://montexanic.mx/tienda/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-monte-xanic.svg" alt="logo-monte-xanic" class="brand"></a>
                    <a href="http://madero.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-casa-madero.svg" alt="logo-casa-madero" class="brand"></a>
                    <a href="http://www.cervezalabru.com.mx/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-la-bru.svg" alt="logo-la-bru" class="brand"></a>
                    <a href="https://sargazo.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-el-sargazo.svg" alt="logo-el-sargazo" class="brand"></a>
                    <a href="https://colegioculinario.edu.mx/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-colegio-culinario.svg" alt="logo-colegio-culinario" class="brand"></a>
                    <a href="https://www.facebook.com/XanequeSE/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-xaneque.svg" alt="logo-xaneque" class="brand"></a>
                    <a href="http://santo-tomas.com/" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-santo-tomas@2x.png" alt="logo-santo-tomas" class="brand"></a>
                    <a href="http://www.lalomita.mx" target="_BLANK"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-lomita@2x.png" alt="logo-lomita" class="brand"></a>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-uasisi.svg" alt="logo-uasisi" class="brand">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-tio-ro.svg" alt="logo-tio-ro" class="brand">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-hielos.svg" alt="logo-hielos" class="brand">
                </div>
            </div>
        </div>
    </div>
    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
        <div class="row py4">
            <div class="col-sm-12">
                <?php echo do_shortcode('[instagram-feed]'); ?>
            </div>
        </div>
    </div>

</div><!-- #page-wrapper -->

<?php
get_footer();