<?php
add_action('wp_ajax_meb_load_events_list', 'meb_load_events_list');
add_action('wp_ajax_nopriv_meb_load_events_list', 'meb_load_events_list');

function meb_load_events_list(){
    $venue_id = $_POST['venue_id'];
    $request = new WP_REST_Request( 'GET', '/meb/v1/events/venues/'.$venue_id );
    // $request->set_query_params( [ 'venue_id' => $venue_id ] );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $venue = $response->status == 200 ? $server->response_to_data( $response, false ) : array();
    
    $day_events = array();
    foreach($venue['related_events'] as $event) {
        $day_events[$event['date_data']['start_date']['day']][] = $event;
    }
    ?>

    <?php if(!empty($venue['day_menus'])):
            foreach($venue['day_menus'] as $day_menu): ?>
                <div class="row">
                    <div class="col offset-lg-1">
                        <div class="day-menu">
                            <span class="day-menu-label">Menú del día:</span> <a href="<?php echo $day_menu['permalink'] ?>"><?php echo $day_menu['name'] ?></a>
                        </div>
                    </div>
                </div>
            <?php endforeach;
    endif; ?>

    <?php if(!empty($day_events)): ?>
        <div class="row pt-5 pb-2">
            <div class="col offset-lg-1">
                <strong class="entry-subtitle">Eventos especiales:</strong>
            </div>
        </div>
        <?php foreach($day_events as $day => $events):?>
            <div class="row pb-2">
                <div class="col offset-lg-1">
                    <strong class="event-day-label"><?php echo $day; ?></strong>
                </div>
            </div>
            <?php foreach($events as $event):?>
                <div class="row justify-content-center pb-5 pt-4">
                    <div class="col-6 col-lg-2">
                        <div class="event-img" style="background-image: url('<?php echo $event['image']; ?>')"></div>
                    </div>
                    <div class="col-lg-8">
                        <div class="event-name">
                            <a href="<?php echo $event['permalink']; ?>"><?php echo $event['name']; ?></a>
                        </div>
                        <div class="event-hour">
                            <?php echo $event['date_data']['start_date']['hour']; ?> h
                        </div>
                        <div class="event-categories">
                            <?php echo implode(', ', $event['categories']); ?>
                        </div>
                        <?php foreach($event['guests'] as $guest):?>
                            <div class="event-guest">
                                Invitado: <a href="<?php echo $guest['permalink'] ?>"><?php echo $guest['name'] ?></a>
                            </div>
                        <?php endforeach; ?>
                        <div class="event-description mt-1 py-1">
                            <?php echo $event['description'] ?>
                        </div>
                        <div class="event-tickets">
                            <a href="<?php echo $event['permalink']; ?>#comprar" class="btn btn-outline-primary">Comprar boleto</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php
        endforeach;
    endif;
    wp_die(); 
}

function meb_loader(){
    ?>
    <div class="meb-loader">
        <ul>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="wineglass left">
            <div class="top"></div>
        </div>
        <div class="wineglass right">
            <div class="top"></div>
        </div>
        <div class="loading-text">Cargando</div>
    </div>
    <?php
}

add_filter('tribe_get_event_taxonomy', 'meb_taxonomy_links', 10, 3);
function meb_taxonomy_links( $taxonomy, $post_id, $args ){
 return strip_tags( $taxonomy );
}

add_filter('tribe_get_venue_link', 'meb_venue_link', 10, 4);
function meb_venue_link( $link, $venue_id, $full_link, $url ){
    $link = get_permalink( get_page_by_path( 'eventos' ) ).'?venue_id='.$venue_id;
    return $link;
}

add_filter('tribe_tickets_ticket_block_submit_classes', 'meb_tickets_ticket_block_submit_classes', 10);
function meb_tickets_ticket_block_submit_classes( $classes ){
   
    $classes= ['btn', 'btn-outline-primary', 'tribe-tickets__buy'];
    return $classes;
}

add_filter('tribe_get_events_link', 'meb_get_events_link', 10);
function meb_get_events_link( $link ){
    $link = get_permalink( get_page_by_path( 'eventos' ) );
    return $link;
}

add_filter('tribe_get_venue_link', 'meb_get_venue_link', 10, 4);
function meb_get_venue_link( $link, $venue_id, $full_link, $url ){
    
    $link = !$full_link ? get_permalink( get_page_by_path( 'eventos' ) ).'?venue_id='.$venue_id : $link;
    return $link;
}

add_filter('woocommerce_return_to_shop_redirect', 'meb_return_to_shop_redirect', 10);
function meb_return_to_shop_redirect( $link ){
    $link = home_url('#eventos');
    return $link;
}

add_action( 'wp_enqueue_scripts', function() {

    //this is based on using the "skeleton styles" option
    $styles = [
        'tribe-events-bootstrap-datepicker-css',
        'tribe-events-calendar-style',
        'tribe-events-custom-jquery-styles',
        'tribe-events-calendar-style',
        'tribe-events-calendar-pro-style',
        'tribe-events-full-calendar-style-css',
        'tribe-common-skeleton-style-css',
        'tribe-tooltip',
        'tribe-accessibility-css',
        'event-tickets-plus-tickets-css',
        'event-tickets-tickets-rsvp-css',
        'event-tickets-rsvp',
        'event-tickets-tpp-css',
    ];

    $scripts = [
       "tribe-common",
       "tribe-admin-url-fragment-scroll",
       "tribe-buttonset",
       "tribe-dependency",
       "tribe-pue-notices",
       "tribe-validation",
       "tribe-timepicker",
       "tribe-jquery-timepicker",
       "tribe-dropdowns",
       "tribe-attrchange",
       "tribe-bumpdown",
       "tribe-datatables",
       "tribe-migrate-legacy-settings",
       "tribe-admin-help-page",
       "tribe-tooltip-js",
       "mt-a11y-dialog",
       "tribe-dialog-js",
       "tribe-moment",
       "tribe-tooltipster",
       "tribe-events-settings",
       "tribe-events-php-date-formatter",
       "tribe-events-jquery-resize",
       "tribe-events-chosen-jquery",
       "tribe-events-bootstrap-datepicker",
       "tribe-events-ecp-plugins",
       "tribe-events-editor",
       "tribe-events-dynamic",
       "jquery-placeholder",
       "tribe-events-calendar-script",
       "tribe-events-bar",
       "the-events-calendar",
       "tribe-events-ajax-day",
       "tribe-events-list",
       "tribe-query-string",
       "tribe-clipboard",
       "datatables",
       "tribe-select2",
       "tribe-utils-camelcase",
       "tribe-app-shop-js"
    ];

    wp_deregister_script($scripts);

    wp_deregister_style($styles);

}, 99);


/**
 * Control de cortesías
 */

function meb_check_courtesy_tickets() {
    //productos (tickets) tipo cortesía
    $tickets = new WP_Query( array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'fields' => 'ids',
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => 'cortesia',
            )
        ),

    ) );

    $targeted_ids = $tickets->posts;

    $coupon_applieds = WC()->cart->get_applied_coupons();
    $coupons_count = count($coupon_applieds);
    $check_coupon = false;
    foreach( WC()->cart->get_cart() as $cart_item ) {
        // Check cart item for defined product Ids
        if ( in_array( $cart_item['product_id'], $targeted_ids )  ) {
            $check_coupon = true;
            $coupons_count -= $cart_item['quantity'];
        }
    }
    if($check_coupon && $coupons_count < 0){
         // Clear all other notices          
         wc_clear_notices();

         // Avoid checkout displaying an error notice
         wc_add_notice( 'Las entradas de cortesía requieren un código', 'error' );
         
         // Optional: remove proceed to checkout button
         remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
    }
    
}   
add_action( 'woocommerce_check_cart_items' , 'meb_check_courtesy_tickets', 10, 0 );


//redireccionar detalle de evento (menu) a home
function meb_redirect_day_menu_events() {
    if ( tribe_is_event() && is_single() ) {
        $tribe_ecp   = Tribe__Events__Main::instance();
        $categories = get_the_terms(get_the_ID(), $tribe_ecp->get_event_taxonomy());
        if(!$categories) return;
        foreach($categories as $category){
            if($category->slug == 'menu'){
                wp_redirect( home_url() );
                die;
            }
        }
    }
}
add_action( 'template_redirect', 'meb_redirect_day_menu_events');