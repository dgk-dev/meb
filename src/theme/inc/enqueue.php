<?php
/**
 * UnderStrap enqueue scripts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'meb_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function meb_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		wp_enqueue_style( 'meb-styles', get_template_directory_uri() . '/style.css', array(), $theme_version );

		wp_enqueue_script( 'jquery' );
		if(is_front_page()){
			$gmap_api_key = class_exists( 'Tribe__Events__Main' ) ? tribe_get_option('google_maps_js_api_key', Tribe__Events__Google__Maps_API_Key::$default_api_key) : '';
			wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?&key='.$gmap_api_key, array('jquery'), '', true);
			wp_enqueue_script('googlemaps');
		}
		
		wp_enqueue_script( 'meb-scripts', get_template_directory_uri() . '/js/footer-bundle.js', array('jquery'), $theme_version, true );
		wp_localize_script('meb-scripts', 'mebGlobalObject', array(
			'rest_url' => rest_url(),
			'ajax_url' => admin_url('admin-ajax.php'),
			'theme_url' => get_template_directory_uri()
		));
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // End of if function_exists( 'meb_scripts' ).

add_action( 'wp_enqueue_scripts', 'meb_scripts', 90 );