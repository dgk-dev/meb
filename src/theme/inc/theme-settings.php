<?php
/**
 * Check and setup theme's default settings
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'meb_setup_theme_default_settings' ) ) {
	/**
	 * Store default theme settings in database.
	 */
	function meb_setup_theme_default_settings() {
		$defaults = meb_get_theme_default_settings();
		$settings = get_theme_mods();
		foreach ( $defaults as $setting_id => $default_value ) {
			// Check if setting is set, if not set it to its default value.
			if ( ! isset( $settings[ $setting_id ] ) ) {
				set_theme_mod( $setting_id, $default_value );
			}
		}
	}
}

if ( ! function_exists( 'meb_get_theme_default_settings' ) ) {
	/**
	 * Retrieve default theme settings.
	 *
	 * @return array
	 */
	function meb_get_theme_default_settings() {
		$defaults = array(
			'meb_posts_index_style' => 'default',   // Latest blog posts style.
			'meb_sidebar_position'  => 'right',     // Sidebar position.
			'meb_container_type'    => 'container', // Container width.
		);

		/**
		 * Filters the default theme settings.
		 *
		 * @param array $defaults Array of default theme settings.
		 */
		return apply_filters( 'meb_theme_default_settings', $defaults );
	}
}

add_filter('meb_site_info_content', 'set_meb_footer_info');
if ( ! function_exists( 'set_meb_footer_info' ) ) {
	function set_meb_footer_info(){
		?>
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div id="copyright">
					© 2020 Festival Internacional de Gastronomía y Vino. Todos los derechos reservados.
				</div>
			</div>
			<div class="col-lg-6">
				<nav>
					<ul id="site-info-menu" class="list-inline">
						<li class="footer-item list-inline-item">
							<a class="nav-link" href="/aviso-de-privacidad">Aviso de privacidad</a>
						</li>
						<li class="footer-item list-inline-item">
							<a class="nav-link" href="/terminos-y-condiciones">Términos y condiciones</a>
						</li>
						<li class="footer-item list-inline-item">
							Desarrollado por <a href="http://dgk.com.mx" target="_blank" rel="noopener noreferrer">dgk</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<?php
	}
}

add_filter('nav_menu_css_class', 'set_sitemap_menu_item_class', 10, 4);
if ( ! function_exists( 'set_sitemap_menu_item_class' ) ) {
	function  set_sitemap_menu_item_class($classes , $item, $args, $depth ){
		if($args->theme_location == 'sitemap'){
			$classes[] = 'footer-item';
			$classes[] = 'list-inline-item';
		}

		return $classes;
	}
}

add_filter( 'gettext', 'meb_change_strings', 20, 3 );
if ( ! function_exists( 'meb_change_strings' ) ) {
	function meb_change_strings( $translated_text, $text, $domain ) {
		switch ( $translated_text ) {
			case 'Nombre de usuario o correo electrónico':
				$translated_text = __( 'Correo electrónico', $domain );
			break;
			case 'Nombre de usuario desconocido. Compruébalo de nuevo o inténtalo con tu dirección de correo electrónico.':
				$translated_text = __( 'Por favor comprueba que tu dirección de correo electrónico sea válida.', $domain );
			case 'Cupón: %s':
				$translated_text = __( 'Cortesía: %s', $domain );
			break;
			case 'Código de cupón':
				$translated_text = __( 'Código de cortesía', $domain );
			break;
			case 'Aplicar cupón':
				$translated_text = __( 'Aplicar cortesía', $domain );
			break;
			case 'El código de cupón se ha aplicado correctamente.':
				$translated_text = __( 'El código de cortesía se ha aplicado correctamente.', $domain );
			break;
			case '¡El código del cupón ya se ha aplicado!':
				$translated_text = __( '¡El código de cortesía ya se ha aplicado!', $domain );
			break;
			case 'El cupón ha sido eliminado.':
				$translated_text = __( 'El código de cortesía ha sido eliminado', $domain );
			break;
			case '¡El cupón «%s» no existe!':
				$translated_text = __( '¡La cortesía «%s» no existe!', $domain );
			break;
			case 'Lo siento, este cupón no se puede aplicar a los productos seleccionados.':
				$translated_text = __( 'Lo siento, esta cortesía no se puede aplicar a los eventos seleccionados.', $domain );
			break;
			case '¿Tienes un cupón?':
				$translated_text = __( '¿Tienes una cortesía?', $domain );
			break;
			case 'Si tienes un código de cupón, por favor, aplícalo abajo.':
				$translated_text = __( 'Si tienes un código de cortesía, por favor, aplícalo abajo.', $domain );
			break;
			case 'Se ha alcanzado el límite de uso de cupones.':
				$translated_text = __( 'El código de cortesía ya ha sido usado.', $domain );
			break;
		}
		return $translated_text;
	}
}
add_filter( 'wootickets_ticket_email_subject', 'meb_tickets_email_subject', 20, 2);
if ( ! function_exists( 'meb_tickets_email_subject' ) ) {
	function meb_tickets_email_subject( $subject , $order) {
		$subject = "Tus entradas para Morelia en Boca";
		return $subject;
	}
}

//Google analytics tag
// add_action( 'wp_head', 'meb_google_analytics', 7 );
function meb_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-DQNGR0G9MG"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-DQNGR0G9MG');
    </script>
    <?php
}