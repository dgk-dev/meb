<?php

add_action( 'init', 'meb_custom_post_type_experience' );

function meb_custom_post_type_experience() {
	$labels = array(
		'name'               => _x( 'Experiencia MEB', 'post type general name', 'understrap' ),
		'singular_name'      => _x( 'Experiencia', 'post type singular name', 'understrap' ),
		'menu_name'          => _x( 'Experiencia MEB', 'admin menu', 'understrap' ),
		'add_new'            => _x( 'Añadir nueva', 'experiencia', 'understrap' ),
		'add_new_item'       => __( 'Añadir nueva experiencia', 'understrap' ),
		'new_item'           => __( 'Nueva experiencia', 'understrap' ),
		'edit_item'          => __( 'Editar experiencia', 'understrap' ),
		'view_item'          => __( 'Ver experiencia', 'understrap' ),
		'all_items'          => __( 'Todas las experiencias', 'understrap' ),
		'search_items'       => __( 'Buscar experiencias', 'understrap' ),
		'not_found'          => __( 'No hay experiencias.', 'understrap' ),
		'not_found_in_trash' => __( 'No hay experiencias en la papelera.', 'understrap' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Descripción.', 'understrap' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'exclude_from_search'=> true,
		'show_in_nav_menus'  => true,
		'rewrite' 			 => false,
		'query_var'          => true,
		'capability_type'    => 'page',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' ),
        'menu_icon'          => 'dashicons-format-gallery',
        'menu_position'      => 4,
        'rewrite'            => array( 'slug' => 'experiencia-meb' )
	);

	register_post_type( 'meb-experience', $args );
}

// Metabox data
add_action( 'add_meta_boxes', 'meb_custom_post_type_experience_metaboxes' );
function meb_custom_post_type_experience_metaboxes() {
    add_meta_box(
        'meb-experience-data-metabox',
        __( 'Detalles', 'dgk-theme' ),
        'meb_experience_data_metabox_callback',
        'meb-experience',
        'advanced',
        'high'
    );
}

function meb_experience_data_metabox_callback( $post ){
    wp_nonce_field( basename( __FILE__ ), 'meb-experience-data-nonce' );
    $date = get_post_meta( $post->ID , 'meb-experience-date', true);
    ?>
    <table>
        <tbody class="form-table">
            <tr>
                <th style="font-weight:normal">
                    <label for="meb-experience-date"><?php _e( 'Fecha', 'understrap' )?></label>
                </th>
                <td>
                    <input name="meb-experience-date" type="text" id="meb-experience-date" value="<?php echo $date; ?>" />
                </td>
            </tr>
        </tbody>
    </table>
    <?php
}

//Save all data
add_action( 'save_post', 'meb_custom_post_type_experience_save' );
function meb_custom_post_type_experience_save( $post_id ) {
    // verify nonce
    if (!wp_verify_nonce($_POST['meb-experience-data-nonce'], basename(__FILE__)))
        return $post_id;
        
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
        
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
 
    // Save
    $olddate = get_post_meta($post_id, 'meb-experience-date', true);
    $newdate = $_POST['meb-experience-date']; 
    if ($newdate != $olddate) {
        update_post_meta($post_id, 'meb-experience-date', $newdate);
    }
}

// Remove built in shortcode

add_shortcode( 'gallery', 'modified_gallery_shortcode' );

function modified_gallery_shortcode($attr)
{
    // Replace WP gallery with OWL Carousel using gallery shortcode -- just add `owl=true`
    //
    // [gallery owl="true" link="none" size="medium" ids="378,377,376,375,374,373"]

   
        $attr['itemtag']="span";

        $output = gallery_shortcode($attr);

        $output = strip_tags($output,'<a><img>'); // remove extra tags, but keep these
        $output = str_replace("span", "div", $output); // replace span for div -- removes gallery wrap
        $output = str_replace('gallery-item', "item", $output); // remove class attribute
        $output = str_replace('<a', '<a data-fancybox="meb-galley"', $output); // remove class attribute

        $output = "<div class=\"owl-carousel owl-theme\" >$output</div>"; // wrap in div

        // begin styles and js

        static $js_loaded; // only create once
        if( empty ( $js_loaded )) {

        ob_start();
        ?> 
        <script defer src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script defer src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script>
            jQuery('head').append('<link defer id="owl-carousel-css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" type="text/css" />');
            jQuery('head').append('<link defer id="owl-theme-css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" type="text/css" />');
            jQuery('head').append('<link defer id="prettyphoto-css" rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" type="text/css" />');

            jQuery(document).ready(function () {

                // pulling example from -- including CSS
                // http://owlgraphic.com/owlcarousel/demos/images.html
                jQuery(".owl-carousel a").each(function(){
                    $this = jQuery(this);
                    $this.attr("href", $this.find("img").attr("src"));
                });
                // notice I replaced #id with .class for when you want to have more than one on a page
                jQuery(".owl-carousel").appendTo("footer.entry-footer .meb-experience-gallery");
                jQuery(".owl-carousel").owlCarousel({
                    autoPlay: 3000, //Set AutoPlay to 3 seconds
                    items: 1,
                    nav:true,
                    autoHeight: true,
                    dots: false,
                    navText: ['<span class="gallery-prev-arrow"></span>', '<span class="gallery-next-arrow"></span>']
                });
            });
        </script>
        <?php
        $js_loaded = ob_get_clean(); // store in static var

        // add the HTML output
        $output .= $js_loaded;
        }
    

    return $output; // final html
}