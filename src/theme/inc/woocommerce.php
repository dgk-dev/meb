<?php
/**
 * Add WooCommerce support
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_action( 'after_setup_theme', 'meb_woocommerce_support' );
if ( ! function_exists( 'meb_woocommerce_support' ) ) {
	/**
	 * Declares WooCommerce theme support.
	 */
	function meb_woocommerce_support() {
		add_theme_support( 'woocommerce' );

		// Add Product Gallery support.
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-slider' );

		// Add Bootstrap classes to form fields.
		add_filter( 'woocommerce_form_field_args', 'meb_wc_form_field_args', 10, 3 );
	}
}

// First unhook the WooCommerce content wrappers.
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

// Then hook in your own functions to display the wrappers your theme requires.
add_action( 'woocommerce_before_main_content', 'meb_woocommerce_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'meb_woocommerce_wrapper_end', 10 );

if ( ! function_exists( 'meb_woocommerce_wrapper_start' ) ) {
	/**
	 * Display the theme specific start of the page wrapper.
	 */
	function meb_woocommerce_wrapper_start() {
		$container = get_theme_mod( 'meb_container_type' );
		echo '<div class="wrapper" id="woocommerce-wrapper">';
		echo '<div class="' . esc_attr( $container ) . '" id="content" tabindex="-1">';
		echo '<div class="row">';
		get_template_part( 'global-templates/left-sidebar-check' );
		echo '<main class="site-main" id="main">';
	}
}

if ( ! function_exists( 'meb_woocommerce_wrapper_end' ) ) {
	/**
	 * Display the theme specific end of the page wrapper.
	 */
	function meb_woocommerce_wrapper_end() {
		echo '</main><!-- #main -->';
		get_template_part( 'global-templates/right-sidebar-check' );
		echo '</div><!-- .row -->';
		echo '</div><!-- Container end -->';
		echo '</div><!-- Wrapper end -->';
	}
}

if ( ! function_exists( 'meb_wc_form_field_args' ) ) {
	/**
	 * Filter hook function monkey patching form classes
	 * Author: Adriano Monecchi http://stackoverflow.com/a/36724593/307826
	 *
	 * @param string $args Form attributes.
	 * @param string $key Not in use.
	 * @param null   $value Not in use.
	 *
	 * @return mixed
	 */
	function meb_wc_form_field_args( $args, $key, $value = null ) {
		// Start field type switch case.
		switch ( $args['type'] ) {
			// Targets all select input type elements, except the country and state select input types.
			case 'select':
				/*
				 * Add a class to the field's html element wrapper - woocommerce
				 * input types (fields) are often wrapped within a <p></p> tag.
				 */
				$args['class'][] = 'form-group';
				// Add a class to the form input itself.
				$args['input_class'] = array( 'form-control' );
				// Add custom data attributes to the form input itself.
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
				);
				break;

			/*
			 * By default WooCommerce will populate a select with the country names - $args
			 * defined for this specific input type targets only the country select element.
			 */
			case 'country':
				$args['class'][] = 'form-group single-country';
				break;

			/*
			 * By default WooCommerce will populate a select with state names - $args defined
			 * for this specific input type targets only the country select element.
			 */
			case 'state':
				$args['class'][]           = 'form-group';
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
				);
				break;
			case 'password':
			case 'text':
			case 'email':
			case 'tel':
			case 'number':
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control' );
				break;
			case 'textarea':
				$args['input_class'] = array( 'form-control' );
				break;
			case 'checkbox':
					$args['class'][] = 'form-group';
					// Wrap the label in <span> tag.
					$args['label'] = isset( $args['label'] ) ? '<span class="custom-control-label">' . $args['label'] . '<span>' : '';
					// Add a class to the form input's <label> tag.
					$args['label_class'] = array( 'custom-control custom-checkbox' );
					$args['input_class'] = array( 'custom-control-input' );
				break;
			case 'radio':
				$args['label_class'] = array( 'custom-control custom-radio' );
				$args['input_class'] = array( 'custom-control-input' );
				break;
			default:
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control' );
				break;
		} // End of switch ( $args ).
		return $args;
	}
}

if ( ! is_admin() && ! function_exists( 'wc_review_ratings_enabled' ) ) {
	/**
	 * Check if reviews are enabled.
	 *
	 * Function introduced in WooCommerce 3.6.0., include it for backward compatibility.
	 *
	 * @return bool
	 */
	function wc_reviews_enabled() {
		return 'yes' === get_option( 'woocommerce_enable_reviews' );
	}

	/**
	 * Check if reviews ratings are enabled.
	 *
	 * Function introduced in WooCommerce 3.6.0., include it for backward compatibility.
	 *
	 * @return bool
	 */
	function wc_review_ratings_enabled() {
		return wc_reviews_enabled() && 'yes' === get_option( 'woocommerce_enable_review_rating' );
	}
}

/**
 * Validar/guardar campos de registro
 */

add_action( 'woocommerce_register_post', 'meb_validate_woocommerce_register_fields', 10, 3 );
if ( ! function_exists( 'meb_validate_woocommerce_register_fields' ) ) {
	function meb_validate_woocommerce_register_fields( $username, $email, $errors ) {
		$meb_name = '';
		if(!empty( $_POST['meb_name'] )){
			$meb_name = $_POST['meb_name'];
		}elseif(!empty( $_POST['billing_first_name'] ) && !empty( $_POST['billing_last_name'] )){
			$meb_name = $_POST['billing_first_name'].' '.$_POST['billing_last_name'];
		}
		if ( !$meb_name ) {
			$errors->add( 'meb_name_error', 'El campo <strong>Nombre</strong> es requerido' );
		}

		$meb_phone = '';

		if(!empty($_POST['meb_phone'])){
			$meb_phone = $_POST['meb_phone'];
		}elseif(!empty($_POST['billing_phone'])){
			$meb_phone = $_POST['billing_phone'];
		}
		
		$phone_correct = preg_match('/^[0-9]{10}$/', $meb_phone);
		if (!$phone_correct) {
			$errors->add( 'meb_phone_error', 'El número de teléfono tiene que ser <strong>de 10 dígitos</strong>' );
		}
	}
}

add_action( 'woocommerce_created_customer', 'meb_save_register_fields' );
if ( ! function_exists( 'meb_save_register_fields' ) ) {
	function meb_save_register_fields( $customer_id ){
	
		if ( isset( $_POST['meb_name'] ) ) {
			update_user_meta( $customer_id, 'billing_first_name', wc_clean( $_POST['meb_name'] ) );
			update_user_meta( $customer_id, 'shipping_first_name', wc_clean( $_POST['meb_name'] ) );
		}
		if ( isset( $_POST['meb_phone'] ) ) {
			update_user_meta( $customer_id, 'billing_phone', wc_clean( $_POST['meb_phone'] ) );
			update_user_meta( $customer_id, 'shipping_phone', wc_clean( $_POST['meb_phone'] ) );
		}
	
	}
}


function meb_cart(){
	$cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
	$cart_url = wc_get_cart_url();  // Set Cart URL
	?>
	<div class="meb-cart">
		<a class="cart-contents" href="<?php echo $cart_url ?>">
			<i class="fa fa-shopping-cart"></i>
			<span class="meb-cart-quantity"><?php echo $cart_count ?></span>
		</a>
	</div>
	<?php
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	$cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
	$cart_url = wc_get_cart_url();  // Set Cart URL
	?>
	<a class="cart-contents" href="<?php echo $cart_url ?>">
		<i class="fa fa-shopping-cart"></i>
		<span class="meb-cart-quantity"><?php echo $cart_count ?></span>
	</a>
	<?php
	
	$fragments['a.cart-contents'] = ob_get_clean();
	
	return $fragments;
}

/**
 * Reducir Stock en estatus de orden pendiente 
 */

add_action( 'woocommerce_checkout_order_processed', 'meb_reduce_stock_on_pending', 20, 3 );
function meb_reduce_stock_on_pending($order_id, $posted_data, $order){
    
    if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
        return;
    }
    
    //   $paymethod = $order->payment_method_title;
    //   $orderstat = $order->get_status();

    if( $order->has_status('pending') ){
        foreach ( $order->get_items() as $item ) {
            $product_id = $item->is_type('variable') ? $item->get_variation_id() : $item->get_product_id();
            $item_qty = $item->get_quantity();
            $product = wc_get_product( $product_id );
    
            // $product_stock = $product->get_stock_quantity();

            $result = wc_update_product_stock( $product, $item_qty, 'decrease' );
        }
        $order->update_meta_data( '_stock_reduced', 'yes' );
        $order->save();
    }
}

add_action( 'woocommerce_order_status_pending_to_cancelled', 'meb_restore_order_stock', 10, 1 );
add_action( 'woocommerce_order_status_on-hold_to_cancelled', 'meb_restore_order_stock', 10, 1 );
function meb_restore_order_stock( $order_id ) {
    
    $order = wc_get_order($order_id);
    
    if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
        return;
    }
    
    if($order->get_meta('_stock_reduced') != 'yes') return;
    
    if(!is_admin()) WC()->cart->empty_cart();
    
    foreach ( $order->get_items() as $item ) {
        $product_id = $item->is_type('variable') ? $item->get_variation_id() : $item->get_product_id();

        $product = wc_get_product( $product_id );
		$quantity = $item->get_quantity();
		
		wc_update_product_stock( $product, $quantity, 'increase' );
    }

    $order->update_meta_data( '_stock_reduced', 'restored' );
    $order->save();
}

//Regresa stock al ejecutar cancelación automática de órdenes pendientes de pago
remove_filter( 'woocommerce_cancel_unpaid_orders', 'wc_cancel_unpaid_orders' );
add_filter( 'woocommerce_cancel_unpaid_orders', 'meb_override_cancel_unpaid_orders' );

function meb_override_cancel_unpaid_orders() {
    $held_duration = get_option( 'woocommerce_hold_stock_minutes' );

    if ( $held_duration < 1 || 'yes' !== get_option( 'woocommerce_manage_stock' ) ) {
        return;
    }

    $data_store    = WC_Data_Store::load( 'order' );
    $unpaid_orders = $data_store->get_unpaid_orders( strtotime( '-' . absint( $held_duration ) . ' MINUTES', current_time( 'timestamp' ) ) );

    if ( $unpaid_orders ) {
        foreach ( $unpaid_orders as $unpaid_order ) {
            $order = wc_get_order( $unpaid_order );

            if ( apply_filters( 'woocommerce_cancel_unpaid_order', 'checkout' === $order->get_created_via(), $order ) ) {
                //Cancel Order
                $order->update_status( 'cancelled', __( 'Unpaid order cancelled - time limit reached.', 'woocommerce' ) );

                //Restock
                meb_restore_order_stock($order->get_id());
            }
        }
    }
    wp_clear_scheduled_hook( 'woocommerce_cancel_unpaid_orders' );
	wp_schedule_single_event( time() + ( absint( $held_duration ) * 60 ), 'woocommerce_cancel_unpaid_orders' );
}

// Deshabilita una segunda reducción de stock si ya se hizo en la función meb_reduce_stock_on_pending
add_filter( 'woocommerce_can_reduce_order_stock', 'meb_disable_reduce_stock', 15, 2);
function meb_disable_reduce_stock( $reduce_stock, $order ) {
	return $order->get_meta('_stock_reduced') == 'yes' ? false : $reduce_stock;
}

// Automatic cancel order fix
add_action( 'init', 'meb_cancel_unpaid_orders_fix', 99 );
function meb_cancel_unpaid_orders_fix() {
	if(wp_next_scheduled( 'woocommerce_cancel_unpaid_orders' )) return;
	$held_duration = get_option( 'woocommerce_hold_stock_minutes' );
	wp_schedule_single_event( time() + ( absint( $held_duration ) * 60 ), 'woocommerce_cancel_unpaid_orders' );
}