<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'meb_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">
	<div class="<?php echo esc_attr( $container ); ?>">
		<div class="row">
			<div class="col-sm-12 text-center">
				<div class="footer-sitemap">
					<span class="sitemap-title">Mapa del sitio: </span>
					<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'sitemap',
								'container_class' => '',
								'container_id'    => 'footer-sitemap-menu',
								'menu_class'      => 'list-inline',
								'fallback_cb'     => '',
								'item_spacing'   => 'discard',
								'menu_id'         => 'sitemap-menu',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						);
					?>
				</div>
			
			</div>
		</div>
	</div>

	<div style="background-color: #f8f0e2;">
		<div class="<?php echo esc_attr( $container ); ?>">
			<div class="row">

				<div class="col-sm-12">

					<footer class="site-footer" id="colophon">

						<div class="site-info">

							<?php meb_site_info(); ?>

						</div><!-- .site-info -->

					</footer><!-- #colophon -->

				</div><!--col end -->

			</div><!-- row end -->
		</div>
	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

