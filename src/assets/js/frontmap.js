var mapTheme = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f2ece1"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3a3a3a"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#fff9ee"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#fff8ee"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3a3a3a"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#fff9ee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
];
(function ($) {
    function initMebMap() {
      var map, infoWindow, geolocation, markersArray = [], infoWindow, locationMarker;

      geolocation = {
          lat: 23.5540767,
          lng: -102.6205
      }

      map = new google.maps.Map(document.getElementById('meb-map'), {
          center: geolocation,
          zoom: 5,
          mapTypeControl: false,
          fullscreenControl: false,
          styles: mapTheme
      });

      //infowindow
      infoWindow = new google.maps.InfoWindow();
      google.maps.event.addListener(infoWindow, 'domready', function () {
          $('.gm-ui-hover-effect').addClass(this.getAnchor().infoColor);
      });

      // My location button
      var myLocationControlDiv = document.createElement('div');
      var myLocationControl = new MyLocationControl(myLocationControlDiv, map);

      myLocationControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.BOTTOM].push(myLocationControlDiv);


      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
            geolocation.lat = position.coords.latitude;
            geolocation.lng = position.coords.longitude;
            map.setCenter(geolocation);
            map.setZoom(16);

            if (locationMarker == undefined) {
              locationMarker = new LocationMarker(geolocation.lat, geolocation.lng);
              locationMarker.setMap(map);
            } else {
              locationMarker.pos = new google.maps.LatLng(geolocation.lat, geolocation.lng);
              locationMarker.draw();
            }
            $('#my-location-control').addClass('active');
          });
        } else {
          console.log('Browser doesn\'t support geolocation');
        }
      }

      function loadMarkers() {
        $.ajax({
            type: 'get',
            url: mebGlobalObject.rest_url+'meb/v1/events/venues/',
            dataType: 'json',
            success: function (venues) {
              $.each(venues, function (index, value) {
                var markerLatlng = new google.maps.LatLng(value.geolocation.latitude, value.geolocation.longitude);

                var marker = new google.maps.Marker({
                  position: markerLatlng,
                  name: value.name,
                  permalink: value.permalink,
                  animation: google.maps.Animation.DROP,
                  events: value.related_events,
                  day_menus: value.day_menus,
                  icon: mebGlobalObject.theme_url + '/img/1-e-icon-36-x-36-pin.svg',
                });

                marker.setMap(map);
                markersArray.push(marker);

                google.maps.event.addListener(marker, 'click', function () {
                  infoWindow.close(); // Close previously opened infowindow
                  infoWindow.setContent(setInfoWindowContent(this));
                  infoWindow.open(map, this);
                });
              });
            },
            error: function(xhr, status, error) {
              console.log(xhr.responseText);
            }
        });
      }

      function setInfoWindowContent(marker) {
          var events = marker.events.reduce((r, a) => {
            r[a.date_data.start_date.day] = [...r[a.date_data.start_date.day] || [], a];
            return r;
          }, {});
          
          var venueEvents = '';

          $.each(events, function (day, events){
            var eventsString = '';
            $.each(events, function (index, event){
              eventsString +=
              '<div class="map-event row no-gutters">'+
                '<div class="map-event-hour col-sm-12 col-lg-6">'+
                  event.date_data.start_date.hour + ' h'+
                '</div>'+
                '<div class="map-event-name col-sm-12 col-lg-6">'+
                  '<a href='+event.permalink+'>'+event.name+'</a>'+
                '</div>'+
              '</div>';
            });

            venueEvents +=
            '<div class="venue-events-data">'+
              '<div class="venue-event-day">'+
                day+
              '</div>'+
              eventsString+
            '</div>';

          });

          var day_menus = '';

          if(marker.day_menus.length){
            day_menus += '<div class="row no-gutters">'+
                      '<div class="col-sm-12"><span class="day-menu-title">Menú del día:</span> ';
            $.each(marker.day_menus, function(index, day_menu){
              day_menus += '<a class="day-menu" href="'+day_menu.permalink+'">'+day_menu.name+'</a>';
              day_menus += index+1 < marker.day_menus.length ? ', ' : '';
            });
            day_menus += '</div></div>'
          }

          var contentString =
          '<div id="infowindow-content" class="meb-infowindow">' +
            '<div id="infowindow-content__data">' +
              '<div class="infowindow-venue">'+marker.name+'</div>' +
              '<div class="infowindow-day-menus">'+day_menus+'</div>' +
              '<div class="infowindow-events">'+venueEvents+'</div>' +
              '<div class="infowindow-footer row no-gutters align-items-center">'+
                '<div class="venue-details col-sm-12 col-lg-6">'+
                  '<a href="'+marker.permalink+'">Ver detalles</a>' +
                '</div>' +
                '<div class="venue-place col-sm-12 col-lg-6">'+
                  '<a class="btn btn-outline-primary" target="_BLANK" href="http://www.google.com/maps/place/' + marker.getPosition().lat() + ',' + marker.getPosition().lng() + '">Cómo llegar</a>' +
                '</div>' +
              '</div>' +
            '</div>' +
          '</div>';
          return contentString;
      }
      //Location button
      function MyLocationControl(controlDiv, map) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.id = 'my-location-control';
        controlUI.className = 'btn btn-outline-primary';
        controlUI.textContent = 'Mi ubicación';
        controlUI.title = 'Mi ubicación';
        controlDiv.appendChild(controlUI);
      
        // Set CSS for the control interior.
        var controlIcon = document.createElement('div');
        controlIcon.id = 'my-location-control__icon';
        controlUI.appendChild(controlIcon);
      
        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
          geolocate();
        });
      }
      
      //set location marker overlay
      function LocationMarker(lat, lng) {
        this.lat = lat;
        this.lng = lng;
        this.pos = new google.maps.LatLng(this.lat, this.lng);
      }

      LocationMarker.prototype = new google.maps.OverlayView();
      LocationMarker.prototype.onRemove = function () { }

      LocationMarker.prototype.onAdd = function () {
        var span = document.createElement('SPAN');
        span.className = "location-pulse";
        var panes = this.getPanes();
        panes.overlayImage.appendChild(span);
      }

      LocationMarker.prototype.draw = function () {
        var overlayProjection = this.getProjection();
        var position = overlayProjection.fromLatLngToDivPixel(this.pos);
        var panes = this.getPanes();
        panes.overlayImage.style.left = position.x + 'px';
        panes.overlayImage.style.top = position.y - 30 + 'px';
      }
      loadMarkers();
    }
    if($('#meb-map').length) initMebMap();
})(jQuery);