(function ($) {
  var $offcanvas = $('.offcanvas-collapse');
  $('[data-toggle="offcanvas"]').on('click', function () {
    $offcanvas.toggleClass('open');
    $offcanvas.hasClass('open') ? disableScroll() : enableScroll();
  });

  $('li.scroll-to a').on('click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    var index = url.indexOf("#");
    var target = (index !== -1) ? '#'+url.substring(index + 1) : ''
    if(target && $(target).length){
      if($offcanvas.hasClass('open')){
        enableScroll();
        $offcanvas.toggleClass('open');
      }
      scrollTo(target);
    }else{
      window.location.href = url;
    }
  });

  //dropdown click fix
  $('.dropdown-toggle').each(function(){
    var firstClick = false;
    $(this).on('click', function(){
      var $this = $(this);
      if(!$this.parent().hasClass('show') && !firstClick){
          $this.parent().addClass('show');
          $this.next().addClass('show');
          firstClick = true;
      }
    });
  });

  //dropdown hover
   
  $('#main-menu')
    .on('mouseenter mouseleave','.dropdown',toggleDropdown)
    .on('click', '.dropdown-menu a', toggleDropdown);

  function scrollTo(id) {
      // Scroll
      $('html,body').animate({
          scrollTop: $(id).offset().top
      }, 'slow');
  }

  function disableScroll(){
    var scrollPosition = [
      self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
      self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
    ];
    var $html = $('html'); // it would make more sense to apply this to body, but IE7 won't have that
    $html.data('scroll-position', scrollPosition);
    $html.data('previous-overflow', $html.css('overflow'));
    $html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
  }

  function enableScroll(){
    var $html = $('html');
    var scrollPosition = $html.data('scroll-position');
    $html.css('overflow', $html.data('previous-overflow'));
    window.scrollTo(scrollPosition[0], scrollPosition[1])
  }

  function toggleDropdown (e) {
    if($('.navbar-toggler').is(':visible')) return;
    const _d = $(e.target).closest('.dropdown'),
        _m = $('.dropdown-menu', _d);
    setTimeout(function(){
      const shouldOpen = e.type !== 'click' && _d.is(':hover');
      _m.toggleClass('show', shouldOpen);
      _d.toggleClass('show', shouldOpen);
      $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
    }, e.type === 'mouseleave' ? 300 : 0);
  }

  $(window).load(function(){
    var target = window.location.hash;
    if(target && $(target).length) scrollTo(target);
    $('input#billing_phone').attr('maxlength', 10);
  });
  
})(jQuery);