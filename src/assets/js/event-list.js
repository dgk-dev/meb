(function ($) {
    
    var $eventsContainer = $('#events-list-container');
    var $loader = $('.meb-loader');
    var xhr;
    if ($eventsContainer.length) loadEvents();
    
    var $venueSelect = $('#venue-select');

    $venueSelect.on('click', 'a.dropdown-item', function(e){
        var $this = $(this);
        var venueName = $this.text();
        var venueID = $this.attr('data-venue-id');
        $venueSelect.find('.selected-title').text(venueName);
        $eventsContainer.attr('data-venue-id', venueID);
        loadEvents();
    });



    function loadEvents() {
        var venueID = $eventsContainer.attr('data-venue-id');
        if(xhr && xhr.readyState != 4){
            xhr.abort();
        }
        xhr = $.ajax({
            type: 'post',
            url: mebGlobalObject.ajax_url,
            data : {
                action : 'meb_load_events_list',
                venue_id : venueID
            },
            success: function (response) {
                if(response){
                    var $events = $(response);
                    $eventsContainer.append($events);
                }else{
                    $eventsContainer.append('<div class="text-center">- No se encontraron eventos próximos en esta sede -</div>');
                }
                $loader.hide();
                $venueSelect.find('a.dropdown-item').removeClass('active');
                $('a.dropdown-item[data-venue-id="'+venueID+'"]').addClass('active');
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            beforeSend: function(){
                $eventsContainer.empty();
                $loader.show();
            }
        });
    }
    
})(jQuery);